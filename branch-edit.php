<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/datepicker.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/datepicker.js"></script>
    <script type="text/javascript" src="js/eye.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
    <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>	
<?php
require_once('configuration/checker.php');
$id = $_GET['id'];
	
?>		
</head>
<body>
	<header>
		<a class="home-link" href="dashboard.php">
		<img src="UCPBGEN_LOGO3.png" width="110px" height="110px"></a>
		<a class="home-link" href="news.php">News</a>
		<a class="home-link" href="events.php">Events</a>
		<a class="home-link" href="careers.php">Careers</a>
		<a class="home-link" href="about_us.php">About Us</a>	
		<a class="home-link active" href="branches.php">Branches</a>	
		<a class="home-link" href="payment_center.php">Payment Center</a>
		<a class="home-link" href="products.php">Products</a>			
		<a class="home-link" href="motor-shops.php">Gawa Agad Motor Shops</a>
		<a class="home-link" href="claims.php">Claims</a>	
		<a class="home-link" href="claimList.php">Claim List</a>				
		<a class="home-link" href="view-users.php">App Users</a>		
		<a class="home-link" href="logout.php">Sign out</a>		
	</header>
	<section id="inside-page">
		<h1>UCPB Gen Branches</h1>
		<div class="inside-actions">
			<div class="add-button on-top">
				<a href="branch_add.php">+ ADD NEW</a>
			</div>
			<a href="branches.php">Go back</a>
		</div>
<?php
	    include('configuration/connection.php');
                    $fetch_branches_tb = mysql_query("SELECT * FROM branches_tb where id='$id'");
                        while ($row = mysql_fetch_array($fetch_branches_tb))
                              {								  
							  $name = $row['name'];	
							  $region = $row['region'];								  
							  $address_contact_info = $row['address_contact_info'];	
							  $latitude = $row['Latitude'];	
							  $longitude = $row['Longitude'];	 							  							  
							  }
							  
							  
							  
							  
?>			
		
		
			<form method="post" action="edit_branch_validate.php">
			<sub>*Required Field</sub><br><br>
			<h3><sub>*</sub>Branch</h3>
			<input type="text" name="branch" placeholder="Branch name here"  <?php echo "value='".htmlentities($name, ENT_QUOTES, 'UTF-8')."'";?>  required>
			<h3><sub>*</sub>Region</h3>
			
<?php   

             $region_result = mysql_query("SELECT * FROM region where region != '$region'");//fecth record from region then display as dropdown item list//
                      echo "

                           <select name='region' required>
                             <option value='$region'>$region</option>";
                                  while($row_region = mysql_fetch_array($region_result))
                                       { 
                                         echo "<option value=".$row_region['region'].">".$row_region['region']."</option>";
                                       } 
                                         echo "</select>";
                             /* end region dropdown
							 -------------------------------*/	


?>			
			

			<br><br>
			<h3><sub>*</sub>Address and Contact Info</h3>
			<textarea rows="8" name='address_contact_info'  required placeholder="Address and Contact Info here"><?php echo $address_contact_info; ?> </textarea>
			<br>
			<h3><sub>*</sub>Google maps Coordinate</h3>
			<h3 style="color:green; font-size:13px;">( Latitude
			<input type="text" name="latitude" value="<?php echo $latitude; ?>" placeholder="00.000000" required style="width:130px !important;">	
             , Longitude
			<input type="text" name="longitude" value="<?php echo $longitude; ?>" placeholder="000.000000" required style="width:130px !important;"> )</h3>	

            <input type="hidden" name="id" value="<?php echo $id;  ?>">						
			<hr>	
			<div class="form-controls">
				<div class="add-button on-bottom">
								<a href="#" onclick="document.getElementById('submitID').click(); return false;"   required/>SAVE</a>
								<input type="submit" id="submitID" style="visibility: hidden;" name="submit"  />						
				</div>							
			</div>
						<div class="cancel-custom"><a href="branches.php" />Cancel</a></div>
		</form>

	</section>
</body>
</html>