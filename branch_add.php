<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/datepicker.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/datepicker.js"></script>
    <script type="text/javascript" src="js/eye.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
    <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>	
<?php
require_once('configuration/checker.php');
	
?>		
</head>
<body>
	<header>
		<a class="home-link" href="dashboard.php">
		<img src="UCPBGEN_LOGO3.png" width="110px" height="110px"></a>
		<a class="home-link" href="news.php">News</a>
		<a class="home-link" href="events.php">Events</a>
		<a class="home-link" href="careers.php">Careers</a>
		<a class="home-link" href="about_us.php">About Us</a>	
		<a class="home-link active" href="branches.php">Branches</a>	
		<a class="home-link" href="payment_center.php">Payment Center</a>
		<a class="home-link" href="products.php">Products</a>			
		<a class="home-link" href="motor-shops.php">Gawa Agad Motor Shops</a>
		<a class="home-link" href="claims.php">Claims</a>	
		<a class="home-link" href="claimList.php">Claim List</a>				
		<a class="home-link" href="view-users.php">App Users</a>		
		<a class="home-link" href="logout.php">Sign out</a>		
	</header>
	<section id="inside-page">
		<h1>UCPB Gen Branches</h1>
		<div class="inside-actions">
<!-- 			<div class="add-button on-top">
				<a href="#">+ ADD NEW</a>
			</div> -->
			<a href="branches.php">Go back</a>
		</div>
			<form method="post" action="add_branch_validate.php">
			<sub>*Required Field</sub><br><br>
			<h3><sub>*</sub>Branch</h3>
			<input type="text" name="branch" placeholder="Branch name here" required>
			<h3><sub>*</sub>Region</h3>
            <select name="region" required>
			<option value="">Select</option>
			<option value="Metro Manila">Metro Manila</option>
			<option value="Luzon">Luzon</option>
            <option value="Visayas">Visayas</option>
			<option value="Mindanao">Mindanao</option>
            </select>
			<br><br>
			<h3><sub>*</sub>Address and Contact Info</h3>
			<textarea rows="8" name='address_contact_info' required placeholder="Address and Contact Info here"></textarea>
			<br>
			<h3><sub>*</sub>Google maps Coordinate</h3>
			<h3 style="color:green; font-size:13px;">( Latitude
			<input type="text" name="latitude" placeholder="00.000000" required style="width:130px !important;">	
             , Longitude
			<input type="text" name="longitude" placeholder="000.000000" required style="width:130px !important;"> )</h3>			
			<hr>	
			<div class="form-controls">
				<div class="add-button on-bottom">
								<a href="#" onclick="document.getElementById('submitID').click(); return false;"   required/>SAVE</a>
								<input type="submit" id="submitID" style="visibility: hidden;" name="submit"  />						
				</div>							
			</div>
						<div class="cancel-custom"><a href="branches.php" />Cancel</a></div>
		</form>

	</section>
</body>
</html>