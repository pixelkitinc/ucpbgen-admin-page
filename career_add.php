<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/datepicker.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/datepicker.js"></script>
    <script type="text/javascript" src="js/eye.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
    <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>	
<?php
require_once('configuration/checker.php');
$date =  date("m/d/Y", strtotime('+8 hours'));	
?>		
</head>
<body>
	<header>
		<a class="home-link" href="dashboard.php">
		<img src="UCPBGEN_LOGO3.png" width="110px" height="110px"></a>
		<a class="home-link" href="news.php">News</a>
		<a class="home-link" href="events.php">Events</a>
		<a class="home-link active" href="careers.php">Careers</a>
		<a class="home-link" href="about_us.php">About Us</a>	
		<a class="home-link" href="branches.php">Branches</a>	
		<a class="home-link" href="payment_center.php">Payment Center</a>
		<a class="home-link" href="products.php">Products</a>		
		<a class="home-link" href="motor-shops.php">Gawa Agad Motor Shops</a>
		<a class="home-link" href="claims.php">Claims</a>	
		<a class="home-link" href="claimList.php">Claim List</a>				
		<a class="home-link" href="view-users.php">App Users</a>		
		<a class="home-link" href="logout.php">Sign out</a>		
	</header>
	<section id="inside-page">
		<h1>UCPB Gen Careers</h1>
		<div class="inside-actions">
<!-- 			<div class="add-button on-top">
				<a href="#">+ ADD NEW</a>
			</div> -->
			<a href="careers.php">Go back</a>
		</div>
			<form method="post" action="add_career_validate.php" id="myform">
			<sub>*Required Field</sub>			
			<h3><sub>*</sub>Position</h3>
			<input type="text" name="position" required>
			<h3><sub>*</sub>Date</h3>
				<p>
					<input class="inputDate" id="inputDate" value="<?php echo$date; ?>" name="date" required/>
					<label id="closeOnSelect"><input type="checkbox" /> Close on selection</label>
				</p><br>
			<h3><sub>*</sub>Requirements</h3>
			<textarea rows="8" name='requirement'required></textarea>
			<h3>Description</h3>
			<textarea rows="8" name='description' ></textarea>			
			<hr>	
			<div class="form-controls">
				<div class="add-button on-bottom">
								<a href="#" onclick="document.getElementById('submitID').click(); return false;" />SAVE</a>
								<input type="submit" id="submitID" style="visibility: hidden;" name="submit"  />						
				</div>							
			</div>
						<div class="cancel-custom"><a href="careers.php" />Cancel</a></div>
		</form>
				<script>$("#myform").validator();</script>
	</section>
</body>
</html>