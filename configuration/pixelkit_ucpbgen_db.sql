-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 03, 2014 at 12:23 AM
-- Server version: 5.5.38-MariaDB
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pixelkit_ucpbgen_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us_tb`
--

CREATE TABLE IF NOT EXISTS `about_us_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `company_description` longtext NOT NULL,
  `contact_email` longtext NOT NULL,
  `contact_number` longtext NOT NULL,
  `kalakbay_contact_number` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `about_us_tb`
--

INSERT INTO `about_us_tb` (`id`, `company_description`, `contact_email`, `contact_number`, `kalakbay_contact_number`) VALUES
(1, 'The Company\r\n\r\nUCPB General Insurance Co., Inc. (UCPB GEN), a duly-recognized company under the ISO 9001:2008 Quality Management System, is the general insurance arm of the UCPB Financial Services Group. Its prime commitment is quality insurance protection. It carries traditional and innovative insurance solutions fit to every need: Property, Marine, Motor, Liability and Casualty, Bonds and Suretyship, and Personal Accident. \r\n\r\nWith over 50 strong years to its name, UCPB GEN looks forward to a vigorous and trailblazing performance in the coming years as it reinforces its position as a recognized leader in the general insurance business.', 'inquire.sample@ucpbgen.com', '+632-811-8329', '+632-811-8329');

-- --------------------------------------------------------

--
-- Table structure for table `app_guest`
--

CREATE TABLE IF NOT EXISTS `app_guest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `app_guest`
--

INSERT INTO `app_guest` (`id`, `username`, `password`) VALUES
(1, 'guest', 'guest');

-- --------------------------------------------------------

--
-- Table structure for table `branches_tb`
--

CREATE TABLE IF NOT EXISTS `branches_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  `region` longtext NOT NULL,
  `address_contact_info` longtext NOT NULL,
  `Latitude` longtext NOT NULL,
  `Longitude` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `branches_tb`
--

INSERT INTO `branches_tb` (`id`, `name`, `region`, `address_contact_info`, `Latitude`, `Longitude`) VALUES
(1, 'Alabang', 'Metro Manila', 'Unit 206 2F Sycamore Arcade\r\nAlabang - Zapote Road, Alabang, Muntinlupa City\r\nTel.No. (02) 807-0355 / 552-1319\r\nTelefax: (02) 850-1086\r\nE-mail: alabang@ucpbgen.com\r\nContact Person: Mr. Aries D. Ope&ntilde;a\r\nOpe&ntilde;a\r\nOpe&ntilde;a\r\n        ', '14.428090', '121.022965'),
(2, 'Makati', 'Metro Manila', 'GF OPL Building, 100 C. Palanca st., \r\nLegaspi Village, Makati City\r\nTel: No. (02) 840-2872 / 840-2751\r\nTelefax: (02) 817-3421      \r\nE-mail: makati@ucpbgen.com\r\nContact Person: Mr. Ericson D. Ramos ', '14.555913', '121.019666'),
(3, 'Manila', 'Metro Manila', 'Suite 219 BPI Condominium\r\nPlaza Cervantes, Binondo, Manila\r\nTel.No. (02) 245-6473 / 245-6448\r\nTelefax: (02) 245-6474\r\nE-mail: manila@ucpbgen.com\r\nContact Person: Ms. Joan  E. Aspuria\r\n', '14.596583', '120.976005'),
(4, 'Quezon City', 'Metro Manila', 'Rm. 407 4/F Cedar Executive Building II, 26 Timog Avenue\r\ncor. Scout Tobias st., Quezon City\r\nTel.No. (02) 372-8771\r\nTelefax (02) 372-8770\r\nE-mail: quezoncity@ucpbgen.com\r\nContact Person:Mr. Noli G. Canilao, Jr.  ', '14.635555', '121.031052'),
(5, 'Batangas', 'Luzon', 'Unit 9 & 10 G/F K-Pointe Commercial Center\r\nSabang, Lipa City\r\nTel.No. (043) 312-3313 / 757-3443\r\nTelefax (043) 757-3419\r\nE-mail: batangas@ucpbgen.com\r\nContact Person: Mr. Wilfredo R. Isla    ', '13.954158', '121.165294'),
(6, 'BiÃ±an', 'Luzon', 'Ammar Commercial Center, along NEPA, National Highway\r\nBrgy Sto. Domingo, BiÃ±an, Laguna\r\nTel.No. (02) 520-6726 / (049) 411-4687\r\nTelefax (02) 520-8279\r\nE-mail: binan@ucpbgen.com\r\nContact Person: Ms. Ma. Milagros C. Mariano         ', '14.329650', '121.088626'),
(7, 'Cabanatuan', 'Luzon', '3F VP Building\r\nBrgy. Conception, Maharlika Highway, Cabanatuan City\r\nTel.No. (044) 600-2788\r\nTelefax (044) 464-1878\r\nE-mail: cabanatuan@ucpbgen.com\r\nContact Person: ', '15.473146', '120.956632'),
(8, 'Dagupan', 'Luzon', 'Unit 214 Metro Plaza Complex\r\nAB Fernandez Avenue Dagupan City Pangasinan\r\nTel.No. (075) 515-6786   Telefax  (075) 523-0040\r\nE-mail: dagupan@ucpbgen.com\r\nContact Person: ', '16.044986', '120.341118'),
(9, 'Dau', 'Luzon', 'Suite 201, Angelique Square Commercial Building\r\nMcArthur Highway, Dau, Mabalacat, Pampanga\r\nTel.No. (045) 331-1994 / 892-5882 / 892-5808\r\nTelefax  (045) 624-0837\r\nE-mail: dau@ucpbgen.com\r\nContact Person: Mr. Anthony I.  Arenas', '15.175020', '120.587683'),
(10, 'Legazpi', 'Luzon', '2F UCPB Building\r\nQuezon Avenue, Legazpi City\r\nTel.No. (052) 480-5069\r\nTelefax (052) 480-4648\r\nE-mail: legazpi@ucpbgen.com\r\nMs. Leda L. Lebitania', '13.146820', '123.754761'),
(11, 'Lucena', 'Luzon', 'GF Quezon Avenue cor.Queblar sts., Lucena City\r\nTel. No. (042) 660-7604 / 710-7603\r\nTelefax (042) 373-7603\r\nE-mail: lucena@ucpbgen.com\r\nContact Person: Mr. Ramon  Y.  Fernandez, Jr.', '13.931175', '121.612754'),
(12, 'Naga', 'Luzon', '2F UCPB Building\r\nEvangelista st., Abella, Naga City, Camarines Sur\r\nTel.No. (054) 473-9962  Telefax (054) 811-1076\r\nE-mail: naga@ucpbgen.com\r\nContact Person: Ms. Kristine C. Tuquero', '13.622879', '123.184794'),
(13, 'Olongapo', 'Luzon', '3F Amigo Building\r\n1095 Rizal Avenue, West Tapinac, Olongapo City\r\nTel.No.(047) 224-7852   Telefax (047) 224-7847\r\nE-mail: olongapo@ucpbgen.com\r\nContact Person: Ms. Abby Roque', '14.843341', '120.288963'),
(14, 'Bacolod ', 'Visayas', 'UCPB Gen Business Center\r\nLacson cor. 20th st., Bacolod City\r\nTel.No. (034) 433-2868\r\nTelefax  (034) 433-2869\r\nE-mail: bacolod@ucpbgen.com\r\nContact Persons: Remy Montemayor/Tess Gutierrez', '10.682414', '122.955773'),
(15, 'Cebu', 'Visayas', 'UCPB Gen Business Center\r\nLacson cor. 20th st., Bacolod City\r\nTel.No. (034) 433-2868\r\nTelefax  (034) 433-2869\r\nE-mail: bacolod@ucpbgen.com\r\nContact Persons: Remy Montemayor/Tess Gutierrez\r\n', '10.682414', '122.955773'),
(16, 'Iloilo', 'Visayas', 'Unit N & O, Mezzanine Floor, J & B Building\r\nMabini St., Ilo Ilo City\r\nTel.No. (033) 337-7768\r\nTelefax: (033) 337-6868\r\nE-mail: iloilo@ucpbgen.com\r\nContact Person: Mr. Renato P. Maravilla', '10.697841', '122.561467'),
(17, 'Cagayan de Oro', 'Mindanao', '2F Dupoint Management Corporation Building\r\nA.Velez cor. Cruz-Taal st., Cagayan De Oro City\r\nTel. No. (088) 857-2349 / (08822) 714-095\r\nTelefax (08822) 726-682\r\nContact Person: Mr. Jose Antonio D. Estrella', '8.478259', '124.644173'),
(18, 'Davao', 'Mindanao', 'GF UCPLAC Building\r\nC.M. Recto st. cor. Palma Gil st., Davao City\r\nTel. No. (082) 221-9053\r\nTelefax (082) 221-9054\r\nE-mail: davao@ucpbgen.com\r\nContact Person: Mr. Jeoffrey Palmares', '7.068869', '125.611080'),
(19, 'General Santos', 'Mindanao', '2F RD Plaza Building\r\nPendatun cor. Deproza sts., General Santos City\r\nTel. No. (083) 301-4727\r\nTelefax  (083) 552-9758\r\nE-mail: gensan@ucpbgen.com\r\nContact Person: Mr. Ronilo D. Lubria', '6.112606', '125.170052');

-- --------------------------------------------------------

--
-- Table structure for table `careers_tb`
--

CREATE TABLE IF NOT EXISTS `careers_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `date` varchar(15) DEFAULT NULL,
  `position` longtext NOT NULL,
  `requirement` longtext,
  `description` longtext,
  PRIMARY KEY (`id`),
  KEY `position` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `careers_tb`
--

INSERT INTO `careers_tb` (`id`, `date`, `position`, `requirement`, `description`) VALUES
(16, '07/02/14', 'ACCOUNTING ANALYST', 'Must be a graduate of BS Accountancy and must be a Certified Public Accountant\r\nWith above average oral and written communication skills\r\n  Open to fresh graduates\r\n', '(Accounting Dept.)\r\n*Responsible for providing monthly analysis on losses (GIS), handle production report, booking of movement of outstanding losses under GIS, monthly reconciliation of books of account versus trial balance.\r\n'),
(28, '07/04/2014', 'ARS ASSISTANT', 'Must be a graduate of BS Accountancy/Finance or any business courses\r\nWith above average oral and written communication skills\r\n Open to fresh graduates\r\n', '(Accounts Receivables Section, Finance)\r\n*Responsible for collection of unpaid premiums due from non-problematic unserviced agents/brokers, commission preparation, analyzing and reconciling statement of account balance'),
(29, '07/04/2014', 'BRANCH SERVICES ASSISTANT (PROCESSOR)', 'Candidate must possess a degree in Marketing or any Business course\r\nWith average written and interpersonal communication skills\r\nOpen to fresh graduates \r\nOpen to internal applications \r\n\r\n', '(Alabang Branch)\r\n* Responsible for policy issuance of the branch and its writing agents \r\n'),
(31, '07/04/2014', 'BRANCH SERVICES ASSOCIATE (MARKETING)/ ACCOUNT ASSOCIATE', 'Candidate must possess a degree in Marketing or any Business course\r\nAbove average written and interpersonal communication skills\r\nWith at least 1 year experience in the Marketing/Non-life insurance field or any related field\r\nOpen to fresh graduates \r\nOpen to internal applications \r\n', '* Conducts clients servicing; Solicits business from producers, assists in the development of new and inactive brokers; Monitors accounts renewal of assigned brokers'),
(42, '07/28/2014', 'ACTUARIAL ASSOCIATE', 'Candidate must possess a degree in BS Math/Actuarial Science\r\nAbove average written and interpersonal communication skills\r\nOpen to fresh graduates \r\n\r\n', '(Actuarial Services)\r\n*Responsible in updating underwriting guidelines and authorities as well as in research and developing package products; generates and prepares statistical data for quarterly portfolio analysis\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `claims_tb`
--

CREATE TABLE IF NOT EXISTS `claims_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `gawa_agad_page_content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `claims_tb`
--

INSERT INTO `claims_tb` (`id`, `gawa_agad_page_content`) VALUES
(1, 'Gawa Agad\r\n\r\nGawa Agad is an innovative concept in motor car insurance claims that highly improves claims processing time by authorizing selected repair shops to perform claims administration. Through the Gawa Agad, UCPB GEN realizes its ultimate goal in recognizing customers'' needs and expectations in convenience and quality service.\r\n\r\nWhat to do in case of accident\r\n\r\nFor a hassle free motor repair\r\n\r\nBring your damaged vehicle to any of our GAWA AGAD PROGRAM (GAP) SHOPS\r\n\r\nSubmit the following documents and GAP shop will do the rest:\r\n\r\nInsurance Policy and its Official Receipt\r\nCurrent Driverâ€™s License and its Official Receipt\r\nVehicle''s Certificate of Registration and its Official Receipt\r\nOriginal Police Report and/or Driver''s Affidavit of the accident\r\n\r\nYou can also arrange for the FREE pickup/delivery of your vehicle to/from any of these GAP shops during office hours.\r\n\r\nFor vehicle collision involving third party\r\n\r\nAttend to persons requiring immediate medical attention\r\nSecure a Police Report even for minor damages. Be sure to fully describe the damage to the involved vehicle passenger in order to prevent a larger claim. If possible, take pictures of the damages.\r\n\r\nCount the passengers of other vehicle. Get their names, telephone numbers and the driver''s license number. This is to protect you in case more people will claim for more than the actual number of passengers.\r\nMove vehicles to the roadside in order not to obstruct traffic flow. Both drivers must sign the sketch that describes the vehicle''s position at the time of accident.\r\nNever leave your vehicle unattended to prevent it from further damage.\r\n\r\n\r\n\r\nFor carnapped vehicle\r\n\r\nReport immediately the carnapping incident to the nearest police station. Bring your vehicle''s original Certificates of Registration and Official Receipt (CR and OR) the soonest time possible.\r\nSubmit a copy of the Alarm Sheet prepared by the police station to the Traffic Management Center , Camp Crame, Quezon City.\r\nInform UCPB General Inc. and submit the original and photocopy of the documents mentioned under hassle free motor repair together with the Alarm and Complaint Sheets from the Traffic Management Center (Camp Crame).\r\n\r\nImportant reminder \r\n\r\nThe Own Damage claim against your insurance policy will be denied should you or your authorized representative enter into any form of settlement with the person at fault.\r\n\r\nTo ensure speedy recovery against the person at fault, secure the following from the third party:\r\n\r\n- All the documents mentioned under hassle free motor repair\r\n- Conforme by way of signature to the Driver''s Affidavit of the accident\r\n- Contact numbers (landline, mobile phone, e-mail address)\r\n\r\nYour strict compliance with this reminder will ensure that your participation in the repairs will be borne or at least be minimized.');

-- --------------------------------------------------------

--
-- Table structure for table `claim_list_tb`
--

CREATE TABLE IF NOT EXISTS `claim_list_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  `linked` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `claim_list_tb`
--

INSERT INTO `claim_list_tb` (`id`, `name`, `linked`) VALUES
(1, 'Motor Car ', 'http://www.ucpbgen.com/claims_checklist/Claims%20Document%20Checklist%20_Motor%20Car.pdf'),
(2, 'Fire and Allied Perils ', 'http://www.ucpbgen.com/claims_checklist/Claims%20Checklist%20_Fire%20and%20Allied%20Perils.pdf'),
(3, 'Marine ', 'http://www.ucpbgen.com/claims_checklist/Claims%20Checklist%20_Marine.pdf'),
(4, 'Fidelity ', 'http://www.ucpbgen.com/claims_checklist/Claims%20Checklist%20_%20Fidelity.pdf'),
(5, 'Engineering ', 'http://www.ucpbgen.com/claims_checklist/Claims%20Checklist%20_%20Engineering.pdf'),
(6, 'Miscellaneous Casualty', 'http://www.ucpbgen.com/claims_checklist/Claims%20Checklist%20_Miscellaneous%20Casualty.pdf'),
(7, 'Claims list sample', 'https://docs.google.com/a/pixelkitinc.com/document/d/1MrGhMEPxvhv_h4Led-sbv1B3ewoGsGjLtdIENHc2Z0I/edit#');

-- --------------------------------------------------------

--
-- Table structure for table `events_tb`
--

CREATE TABLE IF NOT EXISTS `events_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `event_title` longtext NOT NULL,
  `description` longtext NOT NULL,
  `date` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `events_tb`
--

INSERT INTO `events_tb` (`id`, `event_title`, `description`, `date`) VALUES
(72, 'Test Event 1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.', '07/30/2014'),
(73, 'Test Event 2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.', '07/30/2014'),
(74, 'Test Event 3', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.', '07/28/2014'),
(76, 'Test Event 4', 'Test Event 4 for testing', '09/05/2014');

-- --------------------------------------------------------

--
-- Table structure for table `gawa_agad_motor_shops_tb`
--

CREATE TABLE IF NOT EXISTS `gawa_agad_motor_shops_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  `region` longtext NOT NULL,
  `city` longtext NOT NULL,
  `address_contact_info` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `gawa_agad_motor_shops_tb`
--

INSERT INTO `gawa_agad_motor_shops_tb` (`id`, `name`, `region`, `city`, `address_contact_info`) VALUES
(3, 'Casa Ciudad, Inc.', 'Metro Manila', 'Quezon City ', '51-53 Kabignayan St., Quezon City \r\nTel. No. 743-47-26 / 412-39-94 / 740-37-41 \r\nFax No. 711-37-57 \r\nContact Person: Kenneth Barretto \r\nE-mail: casa_ciudad@yahoo.com / claims_casaciudad@yahoo.com '),
(9, 'Makati Motorist Auto Center', 'Metro Manila', 'Makati City', '2700 Guatemala St., San Isidro, Makati City \r\nTel. No. 844-10-66 / 844-18-78 \r\nContact Person: Rowel Reginales \r\nE-mail: makati_motorist@yahoo.com \r\n'),
(10, 'Rosemund Motorist Autocenter', 'Metro Manila', 'Quezon City ', 'Lot 3, Blk 2, Arty Rd., Mindanao Ave., \r\nQuezon City \r\nTel. No. 455-68-16 / 930-42-07 \r\nContact Person: Aries Hererra \r\nE-mail: mmacqc@yahoo.com '),
(11, 'New Fix n Paint Auto Body Shop', 'Metro Manila', 'Caloocan City ', '129 Biglang Awa Street corner EDSA, Caloocan City \r\nTel. No. 363-4841 / 360-5091 \r\nContact Person: Anelene Tungol \r\nE-mail: newfixnpaint@yahoo.com '),
(12, 'Makati Motorist Auto Center', 'Metro Manila', 'Bicutan', 'Blk. 2, Lot 8A, Annex 22, Scotland Street, \r\nBetterliving Subdivision, Bicutan \r\nTel. No. 821-68-97 \r\nContact Person: Mr. Rowel Reginales \r\nE-mail: makati_motorist@yahoo.com '),
(13, 'D. Dimaandal Car Care Center', 'Luzon', 'Batangas ', 'Kumintang Ibaba, Batangas City \r\nTelefax (043) 984-18-90 \r\nContact Person: Dante Dimaandal \r\nE-mail: d.dimaandalcarcare@yahoo.com.ph '),
(14, 'MCE Repair Shop', 'Luzon', 'Bataan ', '142 Lote Puerto Rivas, Balanga City, Bataan \r\nTel. No. (047) 237-23-94 / 791-35-77 \r\nContact Person: Modesto Enriquez \r\n Michael Enriquez \r\nE-mail: mcc_repairshop@yahoo.com \r\n'),
(15, 'F1 Alliance', 'Luzon', 'Camarines Sur', 'National Hi-way Zone 5, Del Rosario, Naga City \r\nTel. No. (0920) 318-5840 \r\nContact Person: Ding Candelaria \r\nE-mail: f1_rb@yahoo.com'),
(16, 'Autoline Motors Corp.', 'Luzon', 'Cavite ', 'National Hi-way, Habay, Bacoor \r\nTel. No. (046) 434-1777 / 434-2777 \r\nFax No. (046) 434-1778 \r\nContact Person: Orly Mateo \r\nE-mail: autolinemotorscorp@yahoo.com'),
(17, 'City Fix n Paint Auto Body Shop', 'Luzon', 'Isabela', '1 Patul Road, Bgy. Patul,Santiago City, Isabela \r\nTel. No. (078)-6224020 (Digitel) /( 078)-3530018 (PLDT) \r\n 02-4253615 (Manila PLDT line) \r\nTelefax No. 078-2580618 \r\nContact Person: Jim Gatioan \r\nE-mail:cityfixnpaint@yahoo.com \r\n'),
(18, 'Efren''s Auto Shop', 'Luzon', 'Laguna', 'Brgy. San Vicente San Pablo, Laguna \r\nTel. No. (049) 562-94-43 / 800-10-09 \r\nFax No. (049) 562-82-09 \r\nContact Person: Efren D. Abrigo \r\n Christopher Abrigo \r\nE-mail: senbelleabrigo@yahoo.com '),
(19, 'KGA Auto Restoration', 'Luzon', 'Laguna', 'Brgy. Uno Crossing, National Highway\r\n(near LTO and SM City Calamba) \r\nCalamba City, Laguna \r\nTel. No. (049) 576-1273 \r\nFax No. (049) 545-3680 \r\nContact Person: Claire Aquino \r\nE-mail: engel_kga@yahoo.com '),
(20, 'Ray Junia Cars and Services, Inc.', 'Luzon', 'Laguna', 'National Highway, Brgy. Landayan \r\nSan Pedro Laguna \r\nTel No. 869-1531 / 788-0159 \r\nTelefax No. 869-97-45 \r\nContact Person: Ray Michael Junia \r\nE-mail: rayjunia@hotmail.com'),
(21, 'Car Palace Motor Service', 'Luzon', 'Pampanga ', 'Mc Arthur Hi-way, (infront of Richtown I) \r\nDela Paz Norte, San Fernando, Pampanga \r\nTel No. (045) 861-4921 \r\nFax No. (045) 961-55-66 \r\nContact Person: Edwin Pingol \r\nE-mail: wingpingol@yahoo.com \r\n'),
(22, 'EMD55 Car Center', 'Luzon', 'Laguna', 'Lazatin Blvd., Dolores \r\nCity of San Fernando, Pampanga \r\nTel. No. (045) 961-59-95 \r\nTelefax No. (045) 961-59-94 \r\nContact Person: Edgar Dizon \r\nE-mail: emd55_carcenter@yahoo.com \r\n'),
(23, 'New Hanietipen Marketing and Autoworks', 'Luzon', 'Quezon', 'Maharlika Hi-way, Brgy. Isabang, \r\nLucena City \r\nTel. No. 373-5391 \r\nContact Person: Virgilio Malgapo \r\nE-mail: hanietipenmarketing@yahoo.com'),
(24, 'Cebu Automechanika', 'Visayas', 'Cebu', '13th Ave. cor David Sy Gaisano St. \r\nReclamation Area, Cebu City \r\nTel. No. (032) 233-1077 \r\nFax No. (032) 233-2307 \r\nContact Person: Johniffer P. Enad \r\nE-mail: cebuautomechanika@yahoo.com'),
(25, 'Well Auto Repair Shop', 'Visayas', 'Iloilo', 'San Juan, Molo, Iloilo City \r\nTel. No. (033) 336-4664 \r\nCellphone 0917-7026908 \r\nContact Person: Engr. Louie Q. Abello - Proprietor'),
(26, 'Emmanuel Corporation', 'Visayas', 'Negros Occidental', 'Bangga Subay, Mandalagan, Bacolod City \r\nTel. No. (034) 441-01-07 / 709-81-31 \r\nFax No. (034) 441-01-08 \r\nContact Person: Nestor Jalandoni III, Joe Que, Elsa Patalita, Sally Luberas \r\nE-mail: marymerselect03@yahoo.com '),
(27, 'JB''s', 'Mindanao', 'Bukidnon', 'Sayre Highway, Casisang, Malaybalay City \r\nTelefax No. (088) 221-29-07 \r\nContact Person: Jun Aparece \r\nJigger Sancho \r\nE-mail: qtaparece@mafreinsular.com '),
(28, 'Easycar Auto Repair', 'Mindanao', 'Davao del Norte ', 'Purok 1-A, National Highway \r\nApokon Road, Tagum City \r\nTel. No. (0928) 750-7840 / (084) 216-6568 \r\nContact Person: Jovan Guerra / Erwin Estanislao '),
(29, 'Jiffycar Service Center, Inc.', 'Mindanao', 'Davao del Sur', 'Ecowest Drive, Quimpo Boulevard, \r\nDavao City \r\nTel. No. (082) 297-6088 \r\nFax No. (082) 297-8090 \r\nContact Person: Jenny Bangayan \r\nE-mail: chai.jiffcar@yahoo.com \r\n'),
(30, 'Martini Auto Service Center, Inc.', 'Mindanao', 'Lanao del Norte', 'Andres Bonifacio St., Tibanga Highway \r\nIligan City \r\nTel. No. (063) 221-7228 / 221-5209 \r\nContact Person: Dodong Razuna'),
(31, 'Great White Auto Clinic', 'Mindanao', 'Misamis Oriental', 'Gumamela Extension, Carmen, \r\nCagayan de Oro City \r\nTel. No. (088) 858-27-83 \r\nFax No. (08822) 71-08-76 \r\nContact Person: Mr. Mario Galicano \r\nE-mail: jomarmanubag@yahoo.com \r\nManager: Mr. Rex Avila \r\n'),
(32, 'Karr Jackson Automotive Repair Shop', 'Mindanao', 'Misamis Oriental ', 'OsmeÃ±a-Recto Avenue \r\nCagayan de Oro City \r\nTel. No. (088) 856-2172 / (08822) 714-126 \r\nContact Person: Mr. Mario Galicano \r\nE-mail: karrjackson2005@yahoo.com \r\nManager: Mr. Mario Galicano'),
(33, 'Dumajel Auto Repair', 'Mindanao', 'Zamboanga del Norte ', '232 Mibang, Sta. Filomena \r\nDipolog City \r\nTel. No. (0917) 724-9433 / (065) 212-3761 \r\nContact Person: Ariel Dumajel \r\n'),
(34, 'Auto Specialist Inc. ', 'Metro Manila', 'ParaÃ±aque', 'address: Km. 18 East Service Road (Along SLEX, near Sucat), San Martin de Porres, ParaÃ±aque City\r\nTel No:556-6638\r\nFax No:556-6628\r\nContact Person: Eduardo Zara\r\nE-mail: awaagadkm18@gmail.com\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `img_name_counter`
--

CREATE TABLE IF NOT EXISTS `img_name_counter` (
  `counter` int(15) NOT NULL DEFAULT '1',
  PRIMARY KEY (`counter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `img_name_counter`
--

INSERT INTO `img_name_counter` (`counter`) VALUES
(9);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_user_account`
--

CREATE TABLE IF NOT EXISTS `mobile_user_account` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `username` longtext NOT NULL,
  `password` longtext NOT NULL,
  `firstname` longtext NOT NULL,
  `lastname` longtext NOT NULL,
  `middlename` longtext NOT NULL,
  `birthday` varchar(25) NOT NULL,
  `email_address` longtext NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `address` longtext NOT NULL,
  `tin` varchar(35) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `citizenship` varchar(25) NOT NULL,
  `civil_status` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news_tb`
--

CREATE TABLE IF NOT EXISTS `news_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `news_title` longtext NOT NULL,
  `content` longtext NOT NULL,
  `image` longtext NOT NULL,
  `image_small_version` longtext NOT NULL,
  `image_caption` longtext NOT NULL,
  `image_name` longtext,
  `date` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `content` (`content`(767))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=128 ;

--
-- Dumping data for table `news_tb`
--

INSERT INTO `news_tb` (`id`, `news_title`, `content`, `image`, `image_small_version`, `image_caption`, `image_name`, `date`) VALUES
(109, 'UCPB GEN secures corporate decisions with D&O insurance', 'MAKATI CITY - To secure one''s corporate decisions today, UCPB General Insurance Co., Inc. (UCPBGEN) introduces to the market its newest product - the Directors and Officers (D&O)Liability Insurance - through a cocktail launch held at the Buddha Bar Manila, April 2nd 2014. Top caliber general insurance agents and broker companies graced the affair.\r\n\r\nThe D&O Liability Insurance is designed to protect the personal assets of company executives from claims that may arise from decisions and actions they make within the scope of their regular duties. It also supports good corporate governance by making the risks from the decisions that directors and officers make manageable and transparent.\r\n\r\nD&O Liability Insurance is one of the company''s insurance solutions lined up for a 2014 offering.\r\n\r\nChoose UCPB GEN today.', 'news_images/UCPBGEN_001.jpg', 'small_news_images/UCPBGEN_001_thumb.jpg', 'A FLEXIBLE POLICY WITH BROADER AND MORE RELEVANT COVERAGE. Madeleine Sophie L. Adriano of UCPB GEN Special Lines highlights the key features of the D&O Liability Insurance.', 'small_news_images/UCPBGEN_001_thumb.jpg', '04/04/2014'),
(111, 'IC lauds UCPB GEN', 'Insurance Commissioner Emmanuel F. Dooc commends UCPB General Insurance Company, Inc. (UCPB GEN) for its invaluable involvement in the expeditious handling of claims for the Don Mariano Transit Company Skyway mishap victims. Dooc underscores UCPB GEN''s proaction in\r\n\r\n''leading the way'' in ensuring that the affected parties are able to face their ordeal properly.\r\n\r\nUCPBGEN, the lead company of the Passenger Personal Accident Insurance Program (PPAIP) of Passenger Accident Management &Insurance Agency, Inc. (PAMI), facilitated the release of the insurance benefits to the relatives of the victims. To date, all the claims have been fully settled.\r\n\r\nMeanwhile, the PAMI PPAIP Consortium provided financial assistance to the relatives of the victims of the GV Florida bus accident in Bontoc, Mountain Province through the PAMI PPAIP Consortium, led by PAMI President Eduardo Atayde and coordinated with Insurance Commissioner Dooc and Land Transportation Franchising and Regulatory Board Chairman Winston Ginez.\r\n\r\nUCPBGEN assures the Commission of its continuous service and dedication to the public.', 'news_images/UCPBGEN_003.jpg', 'small_news_images/UCPBGEN_003_thumb.jpg', 'UCPB GEN''s VP-POLA Priscila Manaig talks to bus victims'' families as UCPB GEN SVP Edgardo Rosario looks on.', 'small_news_images/UCPBGEN_003_thumb.jpg', '01/24/2014'),
(112, 'UCPB GEN introduces Trade Credit Insurance', 'MAKATI CITY- UCPB General Insurance Co., Inc. (UCPB GEN) launched its Trade Credit Insurance last June 19, 2013 at the Mandarin Oriental Hotel, highlighting its benefits and key product features.\r\n\r\nBusinesses engage credit facilities to trade goods and services. While Trade Credit is a tool for financial growth, it exposes businesses to new forms of risk which may directly affect their financial health. With \r\n\r\n\r\nUCPB GEN Trade Credit Insurance, owners will no longer worry about payment defaults.\r\n\r\nMr. G. Roy S. Sharma, Managing Director of Asia Reinsurance Brokers, spoke about investing in\r\n\r\nTrade Credit Insurance, showing how this can transform the potentials of a company''s receivables while Department of Trade and Industry (DTI) - Bureau of Export Trade Promotion Director Senen M. Perlada presented the outlook for the Trade Sector in the Philippines.\r\n\r\nUCPB GEN''s Trade Credit Insurance is among its latest innovative insurance solutions introduced to the public on its 50th year. Choose UCPB GEN today.', 'news_images/UCPBGEN_004.jpg', 'small_news_images/UCPBGEN_004_thumb.jpg', '(L-R) Madeleine Sophie L. Adriano, UCPB GEN Underwriting Associate; Edgardo D. Rosario, UCPB GEN Senior Vice President - Controllership Division; Marinella B. Gregorio, UCPB GEN Vice President - Technical and Reinsurance Services Group; Nick Davies, Atradius Re Underwriter; Isabelo P. Africa, UCPB GEN President and CEO; Mandy Phan, Asian Reinsurance Brokers Assistant Director.', 'small_news_images/UCPBGEN_004_thumb.jpg', '06/19/2013'),
(113, 'UCPB GEN commits to Filipino interest', 'MANILA - The Insurance Commission of the Philippines (IC) confers the Certificate of Authority (CA) to UCPB General Insurance Co., Inc. (UCPB GEN), one of the most reliable and best performing insurance providers in the country that has been in existence for 50 years now.\r\n\r\nThe IC is a government entity regulating the industry to ensure the adequate insurance protection of the public. On the other hand, the CA is a seal of assurance for meeting the standards and requirements of the Commission, ensuring the public of the company''s legitimate and qualified transactions. Receiving the CA, public interest becomes a foremost consideration in the operations of UCPBGEN.\r\n\r\nWith 50 years of bringing genuine care and protection to the Filipino people, UCPB GEN trail-blazes with its hassle-free, reliable and committed service. Choose UCPB GEN today.', 'news_images/UCPBGEN_005.jpg', 'small_news_images/UCPBGEN_005_thumb.jpg', 'Conferring the CA: (L-R) Atty. Francisco M. Nob, UCPB GEN Assistant Vice President - Claims and Magdalena A. Parrenas, UCPB GEN Vice President - Finance and Accounting receive the CA from Emmanuel F. Dooc, Insurance Commissioner, with Lourdes E. Cervantes, UCPB GEN Senior Manager - Accounting', 'small_news_images/UCPBGEN_005_thumb.jpg', '07/15/2013'),
(126, 'UCPB GEN: Beyond the Golden Mark and Counting', 'MANILA - UCPB General Insurance Co., Inc. (UCPB GEN) celebrates 51 years of distinctive excellence and service to the Filipino people. The festivities commenced with a distinct start in a production number showcasing the company''s unique talents at "Limang Dekada''t Isa", the company''s anniversary celebration last January 29.\r\n\r\nThe invitation-only, Filipiniana-themed celebration honoring the company''s milestones and its significant contributions to the insurance industry for the past 51 years was held at Palacio de Maynila in the City of Manila.\r\n\r\nTop UCPB GEN Executives headed by the Chairman of its Board of Directors, Atty. Juan Andres D. Bautista together with Managing Director, Atty. Alfredo C. Tumacder, Jr. and President and CEO Mr.Isabelo P. Africa, led the celebration of the company''s over five decades in operations.\r\n\r\nDistinguished guests also graced the momentous occasion.\r\n\r\nFifty-one years is about demonstrating UCPB GEN''s commitment to the insurance industry and the Filipino people and having a bold and positive outlook for 2014. "Our golden year proved that strength and stability can weather the challenges of the times," said Africa.\r\n\r\n"The business of insurance is the business of care. While the quadruple disasters in 2013 squared us in and affected the company''s financial results, UCPB GEN turned these disasters into opportunities to strengthen our corporate citizenship and company esprit de corps. We embarked on Corporate Social Responsibility (CSR) projects for the Typhoon Yolanda survivors. We sent a team to personally hand donations for the affected employees and their relatives. The spirit of volunteerism manifested itself as our employees signed up to do the packing of relief goods for hours at the Department of Social Welfare and Development (DSWD) sent through various institutions," he added.\r\n\r\nA distinguished roster of service awardees were recognized at the affair. Leading the pack is the Head of Bancassurance, Mr. Jonalin S. Tarrayo for her 25 years of dedicated service and long-time partnership with UCPB GEN. New hires from the past year were also formally welcomed in a pinning ceremony led by the company''s key officers.\r\n\r\nBacked by over five decades, UCPB GEN is today at the forefront in the general insurance industry continuously providing innovative products and insurance solutions fit to every need.\r\n', 'news_images/UCPBGEN_002.jpg', 'small_news_images/UCPBGEN_002_thumb.jpg', 'TAGAY SA TAGUMPAY. UCPB GEN Board of Directors and key officers led by Managing Director Atty.Alfredo C. Tumacder, Jr. (fifth from right) join in a ceremonial toast to the company''s future.', 'small_news_images/UCPBGEN_002_thumb.jpg', '01/29/2014'),
(127, 'UCPB GEN enjoins business partners to reach for the stars', 'UCPB General Insurance Co., Inc. (UCPB GEN) introduced yet another inclusive and innovative program for its business partners with the launch of Elevation 51 rewards program. This promo is being offered to reward to all licensed UCPB GEN business partners.\r\n\r\nElevation 51 offers agents of UCPB GEN a new way to enjoy their incentives from sharing business with the company. Stars can be earned by agents during this three-year program and may be redeemed starting March of the succeeding year. Agents earn stars per paid premium while enjoying the best redemption values in the program. Rewards include luxury dining and staycation in exclusive hotels and resorts, air travel, branded home essentials, gadgets and fashion items, among others. An exciting feature of this program lies on the Dream Elevation as UCPB GEN wishes to say, "We help make your dream come true."\r\n\r\nThe Elevation 51 rewards program is distinct from the previous incentives programs offered to UCPB GEN business partners incorporating inventive measures and widens the program scope to fairly cater to all producers.\r\n\r\n"The addition of Elevation 51 to our sales portfolio is great news for our business partners," said UCPB GEN President and CEO Isabelo Africa. "Built on our existing program attributes of distinctive service and innovation, the addition of the Elevation 51 rewards program reinforces our position in the general insurance marketplace, standing toe-to-toe with any incentives program in the industry."\r\n\r\nThe new program gives agents more options, Africa added: "We offer our valued business partners the flexibility to choose their rewards. Whatever the size of their agency in the industry is, we have something in the program in store for them. By delivering flexibility and transparency, UCPB GEN offers the best proposition in the market for its business partners."\r\n\r\nThe grand launch of Elevation 51 was held at the U. P. Ang Bahay ng Alumni in Diliman, Quezon City last July 11, 2014. With over 250 participants in attendance, the event highlighted a number of firsts for UCPB GEN featuring various segments, contests, and presentations which are conceptually new in the industry.\r\n\r\nNationwide branch launches follow thereafter between August and September 2014.', 'news_images/UCPBGEN_006.jpg', 'small_news_images/UCPBGEN_006_thumb.jpg', '', 'UCPBGEN_006.jpg', '07/11/2014');

-- --------------------------------------------------------

--
-- Table structure for table `payment_center_tb`
--

CREATE TABLE IF NOT EXISTS `payment_center_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  `region` longtext NOT NULL,
  `address_contact_info` longtext NOT NULL,
  `Latitude` longtext NOT NULL,
  `Longitude` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `payment_center_tb`
--

INSERT INTO `payment_center_tb` (`id`, `name`, `region`, `address_contact_info`, `Latitude`, `Longitude`) VALUES
(1, 'Alabang', 'Metro Manila', 'Unit 206 2F Sycamore Arcade\r\nAlabang - Zapote Road, Alabang, Muntinlupa City\r\nTel.No. (02) 807-0355 / 552-1319\r\nTelefax: (02) 850-1086\r\nE-mail: alabang@ucpbgen.com\r\nContact Person: Mr. Aries D. OpeÃ±a\r\nOpeÃ±a\r\nOpeÃ±a\r\n    ', '14.428090', '121.022965'),
(2, 'Makati', 'Metro Manila', 'GF OPL Building, 100 C. Palanca st., \r\nLegaspi Village, Makati City\r\nTel: No. (02) 840-2872 / 840-2751\r\nTelefax: (02) 817-3421      \r\nE-mail: makati@ucpbgen.com\r\nContact Person: Mr. Ericson D. Ramos ', '14.555913', '121.019666'),
(3, 'Manila', 'Metro Manila', 'Suite 219 BPI Condominium\r\nPlaza Cervantes, Binondo, Manila\r\nTel.No. (02) 245-6473 / 245-6448\r\nTelefax: (02) 245-6474\r\nE-mail: manila@ucpbgen.com\r\nContact Person: Ms. Joan  E. Aspuria\r\n', '14.596583', '120.976005'),
(4, 'Quezon City', 'Metro Manila', 'Rm. 407 4/F Cedar Executive Building II, 26 Timog Avenue\r\ncor. Scout Tobias st., Quezon City\r\nTel.No. (02) 372-8771\r\nTelefax (02) 372-8770\r\nE-mail: quezoncity@ucpbgen.com\r\nContact Person:Mr. Noli G. Canilao, Jr.', '14.635555', '121.031052'),
(5, 'Batangas ', 'Luzon', 'Unit 9 & 10 G/F K-Pointe Commercial Center\r\nSabang, Lipa City\r\nTel.No. (043) 312-3313 / 757-3443\r\nTelefax (043) 757-3419\r\nE-mail: batangas@ucpbgen.com\r\nContact Person: Mr. Wilfredo R. Isla', '13.954158', '121.165294'),
(6, 'BiÃ±an', 'Luzon', 'Ammar Commercial Center, along NEPA, National Highway\r\nBrgy Sto. Domingo, BiÃ±an, Laguna\r\nTel.No. (02) 520-6726 / (049) 411-4687\r\nTelefax (02) 520-8279\r\nE-mail: binan@ucpbgen.com\r\nContact Person: Ms. Ma. Milagros C. Mariano        ', '14.329650', '121.088626'),
(7, 'Cabanatuan', 'Luzon', '3F VP Building\r\nBrgy. Conception, Maharlika Highway, Cabanatuan City\r\nTel.No. (044) 600-2788\r\nTelefax (044) 464-1878\r\nE-mail: cabanatuan@ucpbgen.com\r\nContact Person: ', '15.473146', '120.956632'),
(8, 'Dagupan', 'Luzon', 'Unit 214 Metro Plaza Complex\r\nAB Fernandez Avenue Dagupan City Pangasinan\r\nTel.No. (075) 515-6786   Telefax  (075) 523-0040\r\nE-mail: dagupan@ucpbgen.com\r\nContact Person: ', '16.044986', '120.341118'),
(9, 'Dau', 'Luzon', 'Suite 201, Angelique Square Commercial Building\r\nMcArthur Highway, Dau, Mabalacat, Pampanga\r\nTel.No. (045) 331-1994 / 892-5882 / 892-5808\r\nTelefax  (045) 624-0837\r\nE-mail: dau@ucpbgen.com\r\nContact Person: Mr. Anthony I.  Arenas', '15.175020', '120.587683'),
(10, 'Legazpi', 'Luzon', '2F UCPB Building\r\nQuezon Avenue, Legazpi City\r\nTel.No. (052) 480-5069\r\nTelefax (052) 480-4648\r\nE-mail: legazpi@ucpbgen.com\r\nMs. Leda L. Lebitania', '13.146820', '123.754761'),
(11, 'Lucena', 'Luzon', 'GF Quezon Avenue cor.Queblar sts., Lucena City\r\nTel. No. (042) 660-7604 / 710-7603\r\nTelefax (042) 373-7603\r\nE-mail: lucena@ucpbgen.com\r\nContact Person: Mr. Ramon  Y.  Fernandez, Jr.', '13.931175', '121.612754'),
(12, 'Naga', 'Luzon', '2F UCPB Building\r\nEvangelista st., Abella, Naga City, Camarines Sur\r\nTel.No. (054) 473-9962  Telefax (054) 811-1076\r\nE-mail: naga@ucpbgen.com\r\nContact Person: Ms. Kristine C. Tuquero', '13.622879', '123.184794'),
(13, 'Olongapo', 'Luzon', '3F Amigo Building\r\n1095 Rizal Avenue, West Tapinac, Olongapo City\r\nTel.No.(047) 224-7852   Telefax (047) 224-7847\r\nE-mail: olongapo@ucpbgen.com\r\nContact Person: Ms. Abby Roque', '14.843341', '120.288963'),
(14, 'Bacolod ', 'Visayas', 'UCPB Gen Business Center\r\nLacson cor. 20th st., Bacolod City\r\nTel.No. (034) 433-2868\r\nTelefax  (034) 433-2869\r\nE-mail: bacolod@ucpbgen.com\r\nContact Persons: Remy Montemayor/Tess Gutierrez', '10.682414', '122.955773'),
(15, 'Cebu', 'Visayas', 'UCPB Gen Business Center\r\nLacson cor. 20th st., Bacolod City\r\nTel.No. (034) 433-2868\r\nTelefax  (034) 433-2869\r\nE-mail: bacolod@ucpbgen.com\r\nContact Persons: Remy Montemayor/Tess Gutierrez\r\n', '10.682414', '122.955773'),
(16, 'Iloilo', 'Visayas', 'Unit N & O, Mezzanine Floor, J & B Building\r\nMabini St., Ilo Ilo City\r\nTel.No. (033) 337-7768\r\nTelefax: (033) 337-6868\r\nE-mail: iloilo@ucpbgen.com\r\nContact Person: Mr. Renato P. Maravilla', '10.697841', '122.561467'),
(17, 'Cagayan de Oro', 'Mindanao', '2F Dupoint Management Corporation Building\r\nA.Velez cor. Cruz-Taal st., Cagayan De Oro City\r\nTel. No. (088) 857-2349 / (08822) 714-095\r\nTelefax (08822) 726-682\r\nContact Person: Mr. Jose Antonio D. Estrella', '8.478259', '124.644173'),
(18, 'Davao', 'Mindanao', 'GF UCPLAC Building\r\nC.M. Recto st. cor. Palma Gil st., Davao City\r\nTel. No. (082) 221-9053\r\nTelefax (082) 221-9054\r\nE-mail: davao@ucpbgen.com\r\nContact Person: Mr. Jeoffrey Palmares', '7.068869', '125.611080'),
(19, 'General Santos', 'Mindanao', '2F RD Plaza Building\r\nPendatun cor. Deproza sts., General Santos City\r\nTel. No. (083) 301-4727\r\nTelefax  (083) 552-9758\r\nE-mail: gensan@ucpbgen.com\r\nContact Person: Mr. Ronilo D. Lubria', '6.112606', '125.170052');

-- --------------------------------------------------------

--
-- Table structure for table `products_tb`
--

CREATE TABLE IF NOT EXISTS `products_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  `category` longtext NOT NULL,
  `preview_text` varchar(60) NOT NULL,
  `full_description` longtext NOT NULL,
  `main_image` longtext NOT NULL,
  `icon` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `products_tb`
--

INSERT INTO `products_tb` (`id`, `name`, `category`, `preview_text`, `full_description`, `main_image`, `icon`) VALUES
(1, 'Fire Insurance', 'Product Lines', 'This covers loss or damage to the insured''s properties', 'This covers loss or damage to the insured''s properties caused by fire and/or natural calamities like typhoon, lightning, flood and earthquake.\r\n\r\nCoverage:\r\nBasic fire and/or lightning\r\nExtraneous perils like\r\nEarthquake\r\nTyphoon\r\nFlood\r\nExplosion\r\nSmoke\r\nVehicle Impact\r\nFalling Aircraft\r\nRiot, Strike and Malicious Damage\r\nIndustrial All Risks\r\nConsequential loss or business interruption\r\nRental Value / Rental Income\r\n', '1.jpg', 'imgIconProductFire.png'),
(2, 'Engineering Insurance', 'Product Lines', 'This provides complete protection against loss of or damage', 'This provides complete protection against loss of or damage to plant, mechanical, electronic and other types of equipment used in construction and/or business operation.\r\nCoverage:\r\nContractor''s All Risks\r\nErection All Risks\r\nIndustrial All Risks\r\nMachinery Breakdown\r\nBoiler and Pressure Vessels\r\nLoss of Profits following Machinery Breakdown\r\nDeterioration of Stocks\r\n', '2.jpg', 'imgIconProductEng.png'),
(3, 'Marine Insurance', 'Product Lines', 'This indemnifies the owner and/or assignee of a vessel', 'This indemnifies the owner and/or assignee of a vessel, plane, goods and/or other transportable properties against sustained loss or damage on land, marine and aerial transit.\r\n\r\nCoverage:\r\nHull\r\nCargo\r\nProperty Floater\r\n', '3.jpg', 'imgIconProductMHull.png'),
(4, 'General Liability', 'Product Lines', 'From risk such as bulgiary to fraud, and the financial', 'Indemnifies the Insured against the financial consequences of accidents to third parties for which he is legally responsible or liable.\r\n\r\nCoverage:\r\nBodily Injury (including death)\r\nLoss of or damage to property\r\nAll costs and expenses of litigation\r\n', '4.jpg', 'imgIconProductLiability.png'),
(5, 'Motor Insurance', 'Product lines', 'This provides financial protection to vehicle owners', 'This provides financial protection to vehicle owners against physical loss of or damage to their vehicles and legal liability to third parties and/or passengers due to accident. Extended perils or optional coverages are also available.\r\n\r\nCoverage:\r\nCompulsory Third Party Liability (CTPL)\r\nOwn Damage/Theft\r\nExcess Third Party Liability (TPL)\r\nAuto Personal Accident (AUPA)\r\nExtended Perils\r\nFlood, Typhoon, Hurricane, Volcanic Eruption, Earthquake\r\nStrikes, Riots and Civil Commotion\r\n', '5.jpg', 'imgIconProductMotor.png'),
(6, 'Bonds & Surety Insurance', 'Product Lines', 'This cover undertakes to provide you with the needed guarant', 'This cover undertakes to provide you with the needed guarantee to complete a contractual or civil engineering project.\r\n\r\nCoverage:\r\n- Fidelity\r\n- Surety\r\n- Judicial\r\n- Performance Bond\r\n', '6.jpg', 'imgIconProductBonds.png'),
(7, 'Personal Accident Insurance', 'Product Lines', 'This provides financial aid to either the insured or his ben', 'This provides financial aid to either the insured or his beneficiaries in case of accidental death or disablement.\r\n\r\nCoverage:\r\nAccidental Death or Disablement\r\nAccidental Medical Reimbursement\r\nMurder and Assault\r\nAccidental Burial Expenses\r\nTypes of Policy:\r\nIndividual Personal Accident (PA)\r\nGroup PA (family, students, employees, etc.)\r\nTravel (local and international)\r\nAuto PA (auto passengers)\r\n', '7.jpg', 'imgIconProductLiability.png'),
(8, 'Directors and Officers Liability Insurance', 'Special Lines', 'Indemnifies the company''s Directors and Officers...', 'Indemnifies the company''s Directors and Officers from claims that may arise from the decisions and actions they make within the scope of their regular duties.\r\n', '8.jpg', 'imgIconProductDirectors.png'),
(9, 'Professional Indemnity Insurance', 'Special Lines', 'Protects Individuals and Companies providing professional ad', 'Protects Individuals and Companies providing professional advice and services from claims made by clients.', '9.jpg', 'imgIconProductProfessional.png'),
(15, 'Trade Credit Insurance', 'Special Lines', 'Protects the Insured Company against non-payment of trade re', 'Protects the Insured Company (supplier/manufacturer/trader) against default or non-payment of trade receivables due to financial, political or economic events which are beyond the company''s control.', '15.jpg', 'imgIconProductTrade.png'),
(16, 'Excel Protect', 'Packages', 'Simple packages comes in extensive coverage.', 'Simple packages comes in extensive coverage. Get an Excel Protect for your home, vehicle, and family today. Package for students and golfers are also available.\r\nHome Excel Plus\r\nHome Excel Protect\r\nMotor Excel Protect\r\nFamily Excel Protect\r\nInternational Travel Protect\r\nGolf Excel Protect\r\nStudent Excel Protect\r\nPro-BIZ\r\n', '16.jpg', 'imgIconProducts.png'),
(17, 'Trucker''s Liability Plus', 'Packages', 'Secures your cargo on board', 'Secures your cargo on board plus personal accident coverage for the named driver.', '17.jpg', 'imgIconProducts.png'),
(18, 'Auto PA Protect', 'Packages', 'Provides you peace of mind while on the road', 'Provides you peace of mind while on the road with your car passengers as well as anywhere you and your family are.\r\n', '18.jpg', 'imgIconProducts.png'),
(19, 'Truckers Cargo Liability Insurance', 'Packages', 'The only cargo liability insurance in the market', 'The only cargo liability insurance in the market which fully covers any loss or damage to the cargo transported due to the insured''s negligent acts and fortuitous events.\r\n', '19.jpg', 'imgIconProducts.png'),
(20, 'Business Insurance package', 'Packages', 'Specially designed insurance package for entrepreneurs', 'Specially designed insurance package for entrepreneurs providing protection and peace of mind. With Pro-Biz mitigating risk, you reduce downtime, protect your investment, and gain the freedom to consolidate or expand your business.\r\n', '20.jpg', 'imgIconProducts.png'),
(21, 'Dentista Segurista Package', 'Packages', 'Adequate insurance coverage for dentists and dental clinics', 'Adequate insurance coverage for dentists and dental clinics, equipment and leasehold improvements as well as patients.\r\n', '21.jpg', 'imgIconProducts.png'),
(22, 'Rural Bank Protect', 'Packages', 'Protection for rural banks nationwide for property damage', 'Protection for rural banks nationwide for property damage, money insurance and fidelity guarantee to secure efficient banking.\r\n', '22.jpg', 'imgIconProducts.png'),
(23, 'Petroleum Outlet Policy', 'Packages', 'Insurance package designed to protect Petroleum Outlets', 'Insurance package designed to protect Petroleum Outlets against various risks of loss.\r\n', '23.jpg', 'imgIconProducts.png'),
(24, 'Pawners Personal Accident Insurance', 'Packages', 'Provides benefits/indemnity in case of loss to the physical ', 'Provides benefits/indemnity in case of loss to the physical well-being of a pawner which arises out of an accident.', '24.jpg', 'imgIconProducts.png'),
(25, 'Constituent Personal Accident Insurance ', 'Packages', 'Government officials insuring their constituents', 'Government officials insuring their constituents under personal accident with various coverages.', '25.jpg', 'imgIconProducts.png'),
(31, 'ksdbnksdbfkbasdkfj', 'Product Lines', 'lasdkflsnadfklnlsdkn', 'askldfh kasdfk hasdfh dfloawefi we isdf  ', '31.jpg', 'imgIconProductProfessional.png'),
(32, 'erioh werlfn dfnk', 'Product Lines', 'sldkfnkjsdb', '.ksdnnf klsdf ksdhfk jhsdfhewfh lwehkf kjbdv ', '32.jpg', 'imgIconProductDirectors.png'),
(34, 'Sample', 'Special Lines', 'sample', 'Hello sample', '34.jpg', 'imgIconProductMotor.png');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `category` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `category`) VALUES
(1, 'Product Lines'),
(2, 'Special Lines'),
(3, 'Packages');

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `region` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `region`) VALUES
(1, 'Metro Manila'),
(2, 'Luzon'),
(3, 'Visayas'),
(4, 'Mindanao');

-- --------------------------------------------------------

--
-- Table structure for table `user_tb`
--

CREATE TABLE IF NOT EXISTS `user_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(35) NOT NULL,
  `lastname` varchar(35) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `firstname` (`firstname`,`lastname`,`username`,`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_tb`
--

INSERT INTO `user_tb` (`id`, `firstname`, `lastname`, `username`, `password`) VALUES
(5, 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(4, 'user', 'user', 'user', 'ee11cbb19052e40b07aac0ca060c23ee');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
