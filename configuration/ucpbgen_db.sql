-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 07, 2014 at 08:42 AM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ucpbgen_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `careers_tb`
--

CREATE TABLE IF NOT EXISTS `careers_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `date` varchar(15) DEFAULT NULL,
  `position` longtext NOT NULL,
  `requirement` longtext,
  `description` longtext,
  PRIMARY KEY (`id`),
  KEY `position` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `careers_tb`
--

INSERT INTO `careers_tb` (`id`, `date`, `position`, `requirement`, `description`) VALUES
(15, '07/02/14', 'ACTUARIAL ASSOCIATE', 'Candidate must possess a degree in BS Math/Actuarial Science\r\nAbove average written and interpersonal communication skills\r\nOpen to fresh graduates \r\n', '(Actuarial Services)\r\n*Responsible in updating underwriting guidelines and authorities as well as in research and developing package products; generates and prepares statistical data for quarterly portfolio analysis\r\n'),
(16, '07/02/14', 'ACCOUNTING ANALYST', 'Must be a graduate of BS Accountancy and must be a Certified Public Accountant\r\nWith above average oral and written communication skills\r\n  Open to fresh graduates\r\n', '(Accounting Dept.)\r\n*Responsible for providing monthly analysis on losses (GIS), handle production report, booking of movement of outstanding losses under GIS, monthly reconciliation of books of account versus trial balance.\r\n'),
(28, '07/04/2014', 'ARS ASSISTANT', 'Must be a graduate of BS Accountancy/Finance or any business courses\r\nWith above average oral and written communication skills\r\n Open to fresh graduates\r\n', '(Accounts Receivables Section, Finance)\r\n*Responsible for collection of unpaid premiums due from non-problematic unserviced agents/brokers, commission preparation, analyzing and reconciling statement of account balance'),
(29, '07/04/2014', 'BRANCH SERVICES ASSISTANT (PROCESSOR)', 'Candidate must possess a degree in Marketing or any Business course\r\nWith average written and interpersonal communication skills\r\nOpen to fresh graduates \r\nOpen to internal applications \r\n\r\n', '(Alabang Branch)\r\n* Responsible for policy issuance of the branch and its writing agents \r\n'),
(31, '07/04/2014', 'BRANCH SERVICES ASSOCIATE (MARKETING)/ ACCOUNT ASSOCIATE', 'Candidate must possess a degree in Marketing or any Business course\r\nAbove average written and interpersonal communication skills\r\nWith at least 1 year experience in the Marketing/Non-life insurance field or any related field\r\nOpen to fresh graduates \r\nOpen to internal applications \r\n', '* Conducts clients servicing; Solicits business from producers, assists in the development of new and inactive brokers; Monitors accounts renewal of assigned brokers'),
(39, '07/07/2014', ' ehem''s', ' ehem''s', ' ehem''s');

-- --------------------------------------------------------

--
-- Table structure for table `events_tb`
--

CREATE TABLE IF NOT EXISTS `events_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `event_title` longtext NOT NULL,
  `description` longtext NOT NULL,
  `date` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `events_tb`
--

INSERT INTO `events_tb` (`id`, `event_title`, `description`, `date`) VALUES
(2, 'Test event part two', 'SURPRISE!!!!!!!!', '07/25/2014'),
(67, 'Mic test! ehemn''s', 'Sound check!!!ehemn''s', '07/07/2014');

-- --------------------------------------------------------

--
-- Table structure for table `news_tb`
--

CREATE TABLE IF NOT EXISTS `news_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `news_title` longtext NOT NULL,
  `content` longtext NOT NULL,
  `image` blob NOT NULL,
  `image_caption` longtext NOT NULL,
  `date` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `content` (`content`(767))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

--
-- Dumping data for table `news_tb`
--

INSERT INTO `news_tb` (`id`, `news_title`, `content`, `image`, `image_caption`, `date`) VALUES
(104, 'UCPB GEN secures corporate decisions with D&O insurance', ' MAKATI CITY - To secure one''s corporate decisions today, UCPB General Insurance Co., Inc. (UCPBGEN) introduces to the market its newest product - the Directors and Officers (D&O)Liability Insurance - through a cocktail launch held at the Buddha Bar Manila, April 2nd 2014. Top caliber general insurance agents and broker companies graced the affair.\r\n         The D&O Liability Insurance is designed to protect the personal assets of company executives from claims that may arise from decisions and actions they make within the scope of their regular duties. It also supports good corporate governance by making the risks from the decisions that directors and officers make manageable and transparent.\r\n         D&O Liability Insurance is one of the company''s insurance solutions lined up for a 2014 offering.\r\n', 0x6e6577735f696d616765732f5543504247454e5f3030312e6a7067, 'A FLEXIBLE POLICY WITH BROADER AND MORE RELEVANT COVERAGE. Madeleine Sophie L. Adriano of UCPB GEN Special Lines highlights the key features of the D&O Liability Insurance.', '07/07/2014'),
(105, 'UCPB GEN: Beyond the Golden Mark and Counting', 'MANILA - UCPB General Insurance Co., Inc. (UCPB GEN) celebrates 51 years of distinctive excellence and service to the Filipino people. The festivities commenced with a distinct start in a production number showcasing the company''s unique talents at "Limang Dekada''t Isa", the company''s anniversary celebration last January 29.\r\n\r\nThe invitation-only, Filipiniana-themed celebration honoring the company''s milestones and its significant contributions to the insurance industry for the past 51 years was held at Palacio de Maynila in the City of Manila.\r\n\r\nTop UCPB GEN Executives headed by the Chairman of its Board of Directors, Atty. Juan Andres D. Bautista together with Managing Director, Atty. Alfredo C. Tumacder, Jr. and President and CEO Mr.Isabelo P. Africa, led the celebration of the company''s over five decades in operations.\r\n\r\nDistinguished guests also graced the momentous occasion.\r\n\r\nFifty-one years is about demonstrating UCPB GEN''s commitment to the insurance industry and the Filipino people and having a bold and positive outlook for 2014. "Our golden year proved that strength and stability can weather the challenges of the times," said Africa.\r\n\r\n"The business of insurance is the business of care. While the quadruple disasters in 2013 squared us in and affected the company''s financial results, UCPB GEN turned these disasters into opportunities to strengthen our corporate citizenship and company esprit de corps. We embarked on Corporate Social Responsibility (CSR) projects for the Typhoon Yolanda survivors. We sent a team to personally hand donations for the affected employees and their relatives. The spirit of volunteerism manifested itself as our employees signed up to do the packing of relief goods for hours at the Department of Social Welfare and Development (DSWD) sent through various institutions," he added.\r\n\r\nA distinguished roster of service awardees were recognized at the affair. Leading the pack is the Head of Bancassurance, Mr. Jonalin S. Tarrayo for her 25 years of dedicated service and long-time partnership with UCPB GEN. New hires from the past year were also formally welcomed in a pinning ceremony led by the company''s key officers.\r\n\r\nBacked by over five decades, UCPB GEN is today at the forefront in the general insurance industry continuously providing innovative products and insurance solutions fit to every need.#', 0x6e6577735f696d616765732f5543504247454e5f3030322e4a5047, 'TAGAY SA TAGUMPAY. UCPB GEN Board of Directors and key officers led by Managing Director Atty.Alfredo C. Tumacder, Jr. (fifth from right) join in a ceremonial toast to the company''s future', '07/07/2014'),
(106, 'IC lauds UCPB GEN', 'Insurance Commissioner Emmanuel F. Dooc commends UCPB General Insurance Company, Inc. (UCPB GEN) for its invaluable involvement in the expeditious handling of claims for the Don Mariano Transit Company Skyway mishap victims. Dooc underscores UCPB GEN''s proaction in\r\n\r\n''leading the way'' in ensuring that the affected parties are able to face their ordeal properly.\r\n\r\nUCPBGEN, the lead company of the Passenger Personal Accident Insurance Program (PPAIP) of Passenger Accident Management &Insurance Agency, Inc. (PAMI), facilitated the release of the insurance benefits to the relatives of the victims. To date, all the claims have been fully settled.\r\n\r\nMeanwhile, the PAMI PPAIP Consortium provided financial assistance to the relatives of the victims of the GV Florida bus accident in Bontoc, Mountain Province through the PAMI PPAIP Consortium, led by PAMI President Eduardo Atayde and coordinated with Insurance Commissioner Dooc and Land Transportation Franchising and Regulatory Board Chairman Winston Ginez.\r\n\r\nUCPBGEN assures the Commission of its continuous service and dedication to the public.#', 0x6e6577735f696d616765732f5543504247454e5f3030332e6a7067, 'UCPB GEN''s VP-POLA Priscila Manaig talks to bus victims'' families as UCPB GEN SVP Edgardo Rosario looks on.', '07/07/2014'),
(107, 'UCPB GEN introduces Trade Credit Insurance', 'MAKATI CITY - UCPB General Insurance Co., Inc. (UCPB GEN) launched its Trade Credit Insurance last June 19, 2013 at the Mandarin Oriental Hotel, highlighting its benefits and key product features.\r\n\r\nBusinesses engage credit facilities to trade goods and services. While Trade Credit is a tool for financial growth, it exposes businesses to new forms of risk which may directly affect their financial health. With UCPB GEN Trade Credit Insurance, owners will no longer worry about payment defaults.\r\n\r\nMr. G. Roy S. Sharma, Managing Director of Asia Reinsurance Brokers, spoke about investing in\r\n\r\nTrade Credit Insurance, showing how this can transform the potentials of a company''s receivables while Department of Trade and Industry (DTI) - Bureau of Export Trade Promotion Director Senen M. Perlada presented the outlook for the Trade Sector in the Philippines.\r\n\r\nUCPB GEN''s Trade Credit Insurance is among its latest innovative insurance solutions introduced to the public on its 50th year. Choose UCPB GEN today.#', 0x6e6577735f696d616765732f5543504247454e5f3030342e6a7067, '(L-R) Madeleine Sophie L. Adriano, UCPB GEN Underwriting Associate; Edgardo D. Rosario, UCPB GEN Senior Vice President - Controllership Division; Marinella B. Gregorio, UCPB GEN Vice President - Technical and Reinsurance Services Group; Nick Davies, Atradius Re Underwriter; Isabelo P. Africa, UCPB GEN President and CEO; Mandy Phan, Asian Reinsurance Brokers Assistant Director.', '07/07/2014'),
(108, 'UCPB GEN commits to Filipino interest', 'MANILA - The Insurance Commission of the Philippines (IC) confers the Certificate of Authority (CA) to UCPB General Insurance Co., Inc. (UCPB GEN), one of the most reliable and best performing insurance providers in the country that has been in existence for 50 years now.\r\n\r\nThe IC is a government entity regulating the industry to ensure the adequate insurance protection of the public. On the other hand, the CA is a seal of assurance for meeting the standards and requirements of the Commission, ensuring the public of the company''s legitimate and qualified transactions. Receiving the CA, public interest becomes a foremost consideration in the operations of UCPBGEN.\r\n\r\nWith 50 years of bringing genuine care and protection to the Filipino people, UCPB GEN trail-blazes with its hassle-free, reliable and committed service. Choose UCPB GEN today.#', 0x6e6577735f696d616765732f5543504247454e5f3030352e6a7067, ' Conferring the CA: (L-R) Atty. Francisco M. Nob, UCPB GEN Assistant Vice President - Claims and Magdalena A. Parrenas, UCPB GEN Vice President - Finance and Accounting receive the CA from Emmanuel F. Dooc, Insurance Commissioner, with Lourdes E. Cervantes, UCPB GEN Senior Manager - Accounting', '07/07/2014');

-- --------------------------------------------------------

--
-- Table structure for table `user_tb`
--

CREATE TABLE IF NOT EXISTS `user_tb` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(35) NOT NULL,
  `lastname` varchar(35) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `firstname` (`firstname`,`lastname`,`username`,`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_tb`
--

INSERT INTO `user_tb` (`id`, `firstname`, `lastname`, `username`, `password`) VALUES
(5, 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(4, 'user', 'user', 'user', 'ee11cbb19052e40b07aac0ca060c23ee');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
