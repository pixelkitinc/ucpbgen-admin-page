<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/datepicker.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/datepicker.js"></script>
    <script type="text/javascript" src="js/eye.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
    <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>	
<?php
require_once('configuration/checker.php');
$id = $_GET['id'];	
?>		
</head>
<body>
	<header>
		<a class="home-link" href="dashboard.php">
		<img src="UCPBGEN_LOGO3.png" width="110px" height="110px"></a>
		<a class="home-link" href="news.php">News</a>
		<a class="home-link" href="events.php">Events</a>
		<a class="home-link" href="careers.php">Careers</a>
		<a class="home-link" href="about_us.php">About Us</a>	
		<a class="home-link" href="branches.php">Branches</a>	
		<a class="home-link" href="payment_center.php">Payment Center</a>
		<a class="home-link" href="products.php">Products</a>		
		<a class="home-link active" href="motor-shops.php">Gawa Agad Motor Shops</a>
		<a class="home-link" href="claims.php">Claims</a>	
		<a class="home-link" href="claimList.php">Claim List</a>				
		<a class="home-link" href="view-users.php">App Users</a>		
		<a class="home-link" href="logout.php">Sign out</a>		
	</header>
	<section id="inside-page">
		<h1>UCPB Gen Gawa Agad Motor Shops</h1>
		<div class="inside-actions">
<!-- 			<div class="add-button on-top">
				<a href="#">+ ADD NEW</a>
			</div> -->
			<a href="motor-shops.php">Go back</a>
		</div>
<?php
	    include('configuration/connection.php');
                    $fetch_motor_shop_tb = mysql_query("SELECT * FROM gawa_agad_motor_shops_tb where id='$id'");
                        while ($row = mysql_fetch_array($fetch_motor_shop_tb))
                              {								  
							  $name = $row['name'];									  
							  $region = $row['region'];	
							  $city = $row['city'];	
							  $address_contact_info = $row['address_contact_info'];	 							  							  
							  }
							  
							  
							  
							  
?>				
		
			<form method="post" action="edit-motor-shops-val.php">
			<sub>*Required Field</sub><br><br>
			<h3><sub>*</sub>Name</h3>
			<input type="text" name="name" placeholder="motor shop name here"  <?php echo "value='".htmlentities($name, ENT_QUOTES, 'UTF-8')."'";?>  required>
			<h3><sub>*</sub>Region</h3>
<?php   

             $region_result = mysql_query("SELECT * FROM region where region != '$region'");//fecth record from region then display as dropdown item list//
                      echo "

                           <select name='region' required>
                             <option value='$region'>$region</option>";
                                  while($row_region = mysql_fetch_array($region_result))
                                       { 
                                         echo "<option value=".$row_region['region'].">".$row_region['region']."</option>";
                                       } 
                                         echo "</select>";
                             /* end region dropdown
							 -------------------------------*/	


?>		
			<br><br>
			<h3><sub>*</sub>City</h3>
			<input type="text" name="city" placeholder="city"  <?php echo "value='".htmlentities($city, ENT_QUOTES, 'UTF-8')."'";?> required>			
			<br><br>
			<h3><sub>*</sub>Address and Contact Info</h3>
			<textarea rows="8" name='address_contact_info' required placeholder="Address and Contact Info here"><?php echo $address_contact_info; ?></textarea>
			<input type="hidden" name="id" value="<?php echo $id; ?>"/>
			
			<br>			
			<hr>	
			<div class="form-controls">
				<div class="add-button on-bottom">
								<a href="#" onclick="document.getElementById('submitID').click(); return false;"   required/>SAVE</a>
								<input type="submit" id="submitID" style="visibility: hidden;" name="submit"  />						
				</div>							
			</div>
						<div class="cancel-custom"><a href="motor-shops.php" />Cancel</a></div>
		</form>

	</section>
</body>
</html>