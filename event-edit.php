<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/datepicker.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/datepicker.js"></script>
    <script type="text/javascript" src="js/eye.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
    <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>	
<?php
require_once('configuration/checker.php');	
$id = $_GET['id'];
?>	
</head>
<body>
	<header>
		<a class="home-link" href="dashboard.php">
		<img src="UCPBGEN_LOGO3.png" width="110px" height="110px"></a>
		<a class="home-link" href="news.php">News</a>
		<a class="home-link active" href="events.php">Events</a>
		<a class="home-link" href="careers.php">Careers</a>
		<a class="home-link" href="about_us.php">About Us</a>	
		<a class="home-link" href="branches.php">Branches</a>	
		<a class="home-link" href="payment_center.php">Payment Center</a>
		<a class="home-link" href="products.php">Products</a>		
		<a class="home-link" href="motor-shops.php">Gawa Agad Motor Shops</a>
		<a class="home-link" href="claims.php">Claims</a>	
		<a class="home-link" href="claimList.php">Claim List</a>				
		<a class="home-link" href="view-users.php">App Users</a>		
		<a class="home-link" href="logout.php">Sign out</a>	
	</header>
	<section id="inside-page">
		<h1>UCPB Gen Event</h1>
		<div class="inside-actions">
      		<div class="add-button on-top">
				<a href="event_add.php">+ ADD NEW</a>
			</div> 
			<a href="events.php">Go back</a>
		</div>
<?php
	    include('configuration/connection.php');
                    $fetch_events_tb = mysql_query("SELECT * FROM events_tb where id='$id'");
                        while ($row = mysql_fetch_array($fetch_events_tb))
                              {								  
							  $title= $row['event_title'];	
							  $content= $row['description'];								  
							  $date= $row['date'];	 							  

							  
							  }
?>			
		
		
			<form enctype="multipart/form-data" method="post" action="edit_event_validate.php" id="myform">
			<sub>*Required Field</sub>
			<h3><sub>*</sub>Event Title</h3>
			<input type="text" name="title"  <?php echo "value='".htmlentities($title, ENT_QUOTES, 'UTF-8')."'";?>  required>
			<h3><sub>*</sub>Date</h3>			
				<p>
					<input class="inputDate" id="inputDate" value="<?php echo $date; ?>" name="date" required/>
					<label id="closeOnSelect"><input type="checkbox" /> Close on selection</label>
				</p><br>
			<h3><sub>*</sub>Description</h3>
			<textarea rows="8" name='content' value="<?php echo $content;?>" required><?php echo $content;?></textarea>

			                               <!-- id reference to next page -->
			                                <input type="hidden" name="id" value="<?php echo $id;?>" />  
										
			<hr>	
			<div class="form-controls">
				<div class="add-button on-bottom">
								<a href="#" onclick="document.getElementById('submitID').click(); return false;"   />SAVE</a>
								<input type="submit" id="submitID" style="visibility: hidden;" name="submit"  />						
				</div>							
			</div>
						<div class="cancel-custom"><a href="events.php" />Cancel</a></div>
		</form>
				<script>$("#myform").validator();</script>		
	</section>
</body>
</html>