<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/datepicker.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/datepicker.js"></script>
    <script type="text/javascript" src="js/eye.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
    <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>	

<?php
require_once('configuration/checker.php');	
$id = $_GET['id'];
?>
<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img_prev')
			.attr('src', e.target.result)
			.height(200);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
</script>


<script>
function smallreadURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#small_img_prev')
			.attr('src', e.target.result)
			.height(100);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
</script>


	
</head>
<body>
	<header>
		<a class="home-link" href="dashboard.php">
		<img src="UCPBGEN_LOGO3.png" width="110px" height="110px"></a>
		<a class="home-link active" href="news.php">News</a>
		<a class="home-link" href="events.php">Events</a>
		<a class="home-link" href="careers.php">Careers</a>
		<a class="home-link" href="about_us.php">About Us</a>	
		<a class="home-link" href="branches.php">Branches</a>	
		<a class="home-link" href="payment_center.php">Payment Center</a>
		<a class="home-link" href="products.php">Products</a>		
		<a class="home-link" href="motor-shops.php">Gawa Agad Motor Shops</a>
		<a class="home-link" href="claims.php">Claims</a>	
		<a class="home-link" href="claimList.php">Claim List</a>				
		<a class="home-link" href="view-users.php">App Users</a>		
		<a class="home-link" href="logout.php">Sign out</a>	
	</header>
	<section id="inside-page">
		<h1>UCPB Gen News</h1>
		<div class="inside-actions">
      		<div class="add-button on-top">
				<a href="news_add.php">+ ADD NEW</a>
			</div> 
			<a href="news.php">Go back</a>
		</div>
<?php
	    include('configuration/connection.php');
                    $fetch_news_tb = mysql_query("SELECT * FROM news_tb where id='$id'");
                        while ($row = mysql_fetch_array($fetch_news_tb))
                              {								  
							  $title= $row['news_title'];	
							  $content= $row['content'];								  
							  $date= $row['date'];	 
							  $image= $row['image'];
							  $image_small= $row['image_small_version'];
							  $image_caption= $row['image_caption'];
							  $image_name= $row['image_name'];							  

							  
							  }
?>			
		
		
			<form enctype="multipart/form-data" method="post" action="edit_news_validate.php" id="myform">
			<sub>*Required Field</sub>
			<h3><sub>*</sub>Title</h3>
			<input type="text" name="title"     <?php echo "value='".htmlentities($title, ENT_QUOTES, 'UTF-8')."'";?>  required>
			<h3><sub>*</sub>Date</h3>			
				<p>
					<input class="inputDate" id="inputDate" value="<?php echo$date; ?>" name="date" required/>
					<label id="closeOnSelect"><input type="checkbox" /> Close on selection</label>
				</p><br>
			<h3><sub>*</sub>Content</h3>
			<textarea rows="8" name='content' value="<?php echo $content;?>" required><?php echo $content;?></textarea>
			<h3><sub>*</sub>Featured Photo</h3>	

								<br>	
                 <img id="img_prev" src="<?php echo $image; ?>" alt="your image" style="height:150px;"/>	   
                                <input type="hidden" name="MAX_FILE_SIZE" value="2048000" />   
					<input type="file" id="image"  name="image_file" onchange="readURL(this);"/>
			<h3><sub>*</sub>Post thumbnail (50px X 50px)</h3>

                 <img id="small_img_prev" src="<?php echo $image_small; ?>" alt="no small image"style="height:70px;"/>     
					<input type="file" id="image"  name="small_image_file" onchange="smallreadURL(this);"/>
			                               <!--for separate image name non-blob-->
			                                <input type="hidden" name="image_name" value="<?php echo $image_small;?>" /> 
								
			                               <!-- id reference to next page -->
			                                <input type="hidden" name="id" value="<?php echo $id;?>" />  
			                               <!--when no image chosen-->
			                                <input type="hidden" name="image_option" value="<?php echo $image;?>" />  
                                                        <input type="hidden" name="image_option2" value="<?php echo $image_small;?>" />  		
			<h3>Photo Caption</h3>
			<input type="text" name="image_caption"  <?php echo "value='".htmlentities($image_caption, ENT_QUOTES, 'UTF-8')."'";?>  >											
			<hr>	
			<div class="form-controls">
				<div class="add-button on-bottom">
								<a href="#" onclick="document.getElementById('submitID').click(); return false;"   />SAVE</a>
								<input type="submit" id="submitID" style="visibility: hidden;" name="submit"  />						
				</div>							
			</div>
						<div class="cancel-custom"><a href="news.php" />Cancel</a></div>
		</form>
				<script>$("#myform").validator();</script>		
	</section>
</body>
</html>