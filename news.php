<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
<?php
require_once('configuration/checker.php');	
?>
		<script language="javascript">
function Del()
{
var r=confirm("Are you sure you want to delete?")
if(r==true){return href;}else{return false;}
}
</script>		
</head>
<body>
	<header>
		<a class="home-link" href="dashboard.php">
		<img src="UCPBGEN_LOGO3.png" width="110px" height="110px"></a>
		<a class="home-link active" href="news.php">News</a>
		<a class="home-link" href="events.php">Events</a>
		<a class="home-link" href="careers.php">Careers</a>
		<a class="home-link" href="about_us.php">About Us</a>	
		<a class="home-link" href="branches.php">Branches</a>	
		<a class="home-link" href="payment_center.php">Payment Center</a>
		<a class="home-link" href="products.php">Products</a>		
		<a class="home-link" href="motor-shops.php">Gawa Agad Motor Shops</a>
		<a class="home-link" href="claims.php">Claims</a>	
		<a class="home-link" href="claimList.php">Claim List</a>				
		<a class="home-link" href="view-users.php">App Users</a>		
		<a class="home-link" href="logout.php">Sign out</a>	
	</header>
	<section id="inside-page">
		<h1>UCPB Gen News</h1>
		<div class="add-button on-top">
			<a href="news_add.php">+ ADD NEW</a>
		</div>
		<table>
			<thead>
				<th>Title</th>
				<th>Date posted</th>
				<th>Preview</th>
				<th>Operations</th>
			</thead>
<?php
include('configuration/connection.php');
		
	$sql = mysql_query("SELECT * FROM news_tb ORDER BY date ASC");
	$nr = mysql_num_rows($sql); // Get total of Num rows from the database query
	
	if (isset($_GET['pn'])) { // Get pn from URL vars if it is present
				$pn = preg_replace('#[^0-9]#i', '', $_GET['pn']); // filter everything but numbers for security(new)
				
		//$pn = ereg_replace("[^0-9]", "", $_GET['pn']); // filter everything but numbers for security(deprecated)
							} 
	else { // If the pn URL variable is not present force it to be value of page number 1
				$pn = 1;
		 } 
		 
				//This is where we set how many database items to show on each page 
				$itemsPerPage = 10; 
	
				// Get the value of the last page in the pagination result set
				$lastPage = ceil($nr / $itemsPerPage);
	
	// Be sure URL variable $pn(page number) is no lower than page 1 and no higher than $lastpage
	if ($pn < 1) { // If it is less than 1
					$pn = 1; // force if to be 1
				 } 
	else if ($pn > $lastPage) { // if it is greater than $lastpage
					$pn = $lastPage; // force it to be $lastpage's value
							  }

							  
	// This creates the numbers to click in between the next and back buttons
	// This section is explained well in the video that accompanies this script

	$centerPages = "";
	$sub1 = $pn - 1;
	$sub2 = $pn - 2;
	$add1 = $pn + 1;
	$add2 = $pn + 2;
	
	if ($pn == 1) {
		$centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
		$centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
				  } 
	else if ($pn == $lastPage) {
		$centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
		$centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
							   }
	else if ($pn > 2 && $pn < ($lastPage - 1)) {
		$centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
		$centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
		$centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
		$centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
		$centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
												} 
	else if ($pn > 1 && $pn < $lastPage) {
		$centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
		$centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
		$centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
										}
	// This line sets the "LIMIT" range... the 2 values we place to choose a range of rows from database in our query
	$limit = 'LIMIT ' .($pn - 1) * $itemsPerPage .',' .$itemsPerPage; 
	// Now we are going to run the same query as above but this time add $limit onto the end of the SQL syntax

	$paginationDisplay = ""; // Initialize the pagination output variable
	// This code runs only if the last page variable is ot equal to 1, if it is only 1 page we require no paginated links to display
	if ($lastPage != "1"){
		// This shows the user what page they are on, and the total number of pages
		$paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage. '&nbsp;  &nbsp;  &nbsp; ';

		// If we are not on page 1 we can place the Back button
		if ($pn != 1) {
			$previous = $pn - 1;
			$paginationDisplay .=  '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
					  } 
		// Lay in the clickable numbers display here between the Back and Next links
		$paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
		// If we are not on the very last page we can place the Next button
		if ($pn != $lastPage) {
			$nextPage = $pn + 1;
			$paginationDisplay .=  '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
							  } 
						  }			
	?>
      <div>
	<?php	
		
                    $fetch_news_tb = mysql_query("SELECT * FROM news_tb ORDER BY date ASC $limit");
  							  if($fetch_news_tb)
								{                       
						while ($row = mysql_fetch_array($fetch_news_tb))
                              {							  
							  $date= $row['date'];	 
							  $formated_date = date("F d, Y", strtotime("$date")); 							  
							  echo "<tr>";
							  echo "<td class='table-title'><a href=\"news-edit.php?id=".$row['id']."\">".$row['news_title']."</a></td>";							  
							  echo "<td class='table-date'>".$formated_date."</td>";	
							  $content = $row['content'];
							  echo "<td class='table-preview'>".implode(' ', array_slice(explode(' ', $content), 0, 10)).".....</td>";	
							  echo "<td class='table-operations'><a href=\"news-edit.php?id=".$row['id']."\">Edit</a> | <a Onclick='return Del();' href=\"news-delete.php?id=".$row['id']."\">Delete</a></td>";								  
							  echo "</tr>";								  
							  }
							  }
							  else
							  {
							  echo"&nbsp;&nbsp;&nbsp;no news posted";							  
							  }							  
	?>			

</div>
		</table>
		<div class="table-navigation">
      <div><?php echo $paginationDisplay; ?></div>
		</div>
	</section>
</body>
</html>