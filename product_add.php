<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/datepicker.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/datepicker.js"></script>
    <script type="text/javascript" src="js/eye.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
    <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>	
<?php
require_once('configuration/checker.php');
	
?>	


<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img_prev')
			.attr('src', e.target.result)
			.height(40);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
</script>	
</head>
<body>
	<header>
		<a class="home-link" href="dashboard.php">
		<img src="UCPBGEN_LOGO3.png" width="110px" height="110px"></a>
		<a class="home-link" href="news.php">News</a>
		<a class="home-link" href="events.php">Events</a>
		<a class="home-link" href="careers.php">Careers</a>
		<a class="home-link" href="about_us.php">About Us</a>	
		<a class="home-link" href="branches.php">Branches</a>	
		<a class="home-link" href="payment_center.php">Payment Center</a>
		<a class="home-link active" href="products.php">Products</a>		
		<a class="home-link" href="motor-shops.php">Gawa Agad Motor Shops</a>
		<a class="home-link" href="claims.php">Claims</a>	
		<a class="home-link" href="claimList.php">Claim List</a>				
		<a class="home-link" href="view-users.php">App Users</a>		
		<a class="home-link" href="logout.php">Sign out</a>		
	</header>
	<section id="inside-page">
		<h1>UCPB Gen Products</h1>
		<div class="inside-actions">
<!-- 			<div class="add-button on-top">
				<a href="#">+ ADD NEW</a>
			</div> -->
			<a href="products.php">Go back</a>
		</div>
			<form method="post" enctype="multipart/form-data" action="add_product_val.php">
			<sub>*Required Field</sub><br><br>
			<h3><sub>*</sub>Product Name</h3>
			<input type="text" name="name" placeholder="Product name here" required>
			<h3><sub>*</sub>Product Category</h3>
            <select name="category" required>
			<option value="">Select</option>
			<option value="Product Lines">Product Lines</option>
			<option value="Special Lines">Special Lines</option>
            <option value="Packages">Packages</option>
            </select>
			<br><br>
			<h3><sub>*</sub>Preview Text  (max. 60 characters)</h3>
			<textarea rows="2" name='preview_text' maxlength="60" required placeholder="type preview text here..."></textarea>
			
			<h3><sub>*</sub>Description</h3>
			<textarea rows="8" name='description' required placeholder="type description here..."></textarea>			
			<h3><sub>*</sub>Select Icon:</h3>
            <br>
        <?php
$folder = 'products_icon/';
$filetype = '*.*';
$files = glob($folder.$filetype);
$count = count($files);
 
$sortedArray = array();
for ($i = 0; $i < $count; $i++) {
    $sortedArray[date ('YmdHis', filemtime($files[$i]))] = $files[$i];
}
 
ksort($sortedArray);
echo "<table style='width:800px;border-collapse: separate; border:0px !important;'>";
       echo"<tr>";
$counter = 0;
foreach ($sortedArray as $filename) {

if ($counter % 6 == 0) {
    echo '</tr><tr>';
	    echo "<td style='border:0px !important;'>";		
			
    echo '<input type="radio" name="selected_icon" value="'.$filename.'"><img src="'.$filename.'"   style=" height:40px;"/>';
	    echo "</td>";	
	
	
}
else{

	    echo "<td style='border:0px !important; '>";		
			
    echo '<input type="radio" name="selected_icon" value="'.$filename.'"><img src="'.$filename.'"   style=" height:40px;"/>';
	    echo "</td>";


}		
  $counter++;
  
  
}
	    /* echo "<td style='border:0px !important; '>";		
			
    echo '<input type="radio" name="selected_icon" value=""><div style="font-weight:bold; color:grey; margin-left:40px; margin-top:-25px;">None</div>';
	    echo "</td>"; */

echo "</tr></table>";
							  
						  
	           echo"<h3>Or upload new:</h3><td style='border:0px !important; '><input type='radio' name='selected_icon' value='upload' style='width:90px; margin-left:-20px;'>&nbsp;&nbsp;&nbsp;<img id='img_prev' src='preview.jpg' alt='select sprite icon' style='background-color:#62bd18; height:40px; margin-left:-40px;'/><input type='file'  id='image'  name='image_file' onchange='readURL(this);' style='width:90px;'/>";
	           echo"</td>";						  
							  
        ?>	
			
			
	
			
			<br>		
			<hr>	
			<div class="form-controls">
				<div class="add-button on-bottom">
								<a href="#" onclick="document.getElementById('submitID').click(); return false;"   required/>NEXT</a>
								<input type="submit" id="submitID" style="visibility: hidden;" name="submit"  />						
				</div>							
			</div>
						<div class="cancel-custom"><a href="products.php" />Cancel</a></div>
		</form>

	</section>
</body>
</html>