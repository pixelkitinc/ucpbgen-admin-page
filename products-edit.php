<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="css/datepicker.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="reset.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/datepicker.js"></script>
    <script type="text/javascript" src="js/eye.js"></script>
    <script type="text/javascript" src="js/utils.js"></script>
    <script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>	
	
<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img_prev')
			.attr('src', e.target.result)
			.height(50);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
</script>	
<?php
require_once('configuration/checker.php');
$id = $_GET['id'];	
?>		
</head>
<body>
	<header>
		<a class="home-link" href="dashboard.php">
		<img src="UCPBGEN_LOGO3.png" width="110px" height="110px"></a>
		<a class="home-link" href="news.php">News</a>
		<a class="home-link" href="events.php">Events</a>
		<a class="home-link" href="careers.php">Careers</a>
		<a class="home-link" href="about_us.php">About Us</a>	
		<a class="home-link" href="branches.php">Branches</a>	
		<a class="home-link" href="payment_center.php">Payment Center</a>
		<a class="home-link active" href="products.php">Products</a>		
		<a class="home-link" href="motor-shops.php">Gawa Agad Motor Shops</a>
		<a class="home-link" href="claims.php">Claims</a>	
		<a class="home-link" href="claimList.php">Claim List</a>				
		<a class="home-link" href="view-users.php">App Users</a>		
		<a class="home-link" href="logout.php">Sign out</a>		
	</header>
	<section id="inside-page">
		<h1>UCPB Gen Products</h1>
		<div class="inside-actions">
		<div class="add-button on-top">
				<a href="product_add.php">+ ADD NEW</a>
			</div>
			<a href="products.php">Go back</a>
		</div>
		
<?php
	    include('configuration/connection.php');
                    $fetch_branches_tb = mysql_query("SELECT * FROM products_tb where id='$id'");
                        while ($row = mysql_fetch_array($fetch_branches_tb))
                              {								  
							  $name = $row['name'];									  
							  $full_description = $row['full_description'];	
							  $preview_text = $row['preview_text'];	
							  $category = $row['category'];	 
                              $icon = $row['icon'];							  
							  $main_image = $row['main_image'];
							  }
							  
							  
							  
							  
?>			
		

			<form method="post" enctype="multipart/form-data" action="edit-product-val.php">
			<sub>*Required Field</sub><br><br>
			<h3><sub>*</sub>Product Name</h3>
			<input type="text" name="name" placeholder="Product name here"  <?php echo "value='".htmlentities($name, ENT_QUOTES, 'UTF-8')."'";?> required>
			<h3><sub>*</sub>Product Category</h3>
<?php   


						 /* category dropdown
							 -------------------------------*/	
             $category_result = mysql_query("SELECT * FROM product_category where category != '$category'");//fecth record from product category then display as dropdown item list//
                      echo "

                           <select name='category' required>
                             <option value='$category'>$category</option>";
                                  while($row_category = mysql_fetch_array($category_result))
                                       { 
                                         echo "<option value=".$row_category['category'].">".$row_category['category']."</option>";
                                       } 
                                         echo "</select>";
                             /* end category dropdown
							 -------------------------------*/	


?>		
			<br><br>
			<h3><sub>*</sub>Preview Text  (max. 60 characters)</h3>
			<textarea rows="2" name='preview_text' maxlength="60" required placeholder="type preview text here..."><?php echo $preview_text; ?></textarea>
			
			<h3><sub>*</sub>Description</h3>
			<textarea rows="8" name='description' required placeholder="type description here..."><?php echo $full_description; ?></textarea>	
			<br>
			<h3><sub>*</sub>Currently selected Icon: <img src="<?php echo "products_icon/$icon"; ?>"  style=" height:40px;"/></h3>
            <br>
        <?php
$folder = 'products_icon/';
$filetype = '*.*';
$files = glob($folder.$filetype);
$count = count($files);
 
$sortedArray = array();
for ($i = 0; $i < $count; $i++) {
    $sortedArray[date ('YmdHis', filemtime($files[$i]))] = $files[$i];
}
 
ksort($sortedArray);
echo "<table style='width:800px;border-collapse: separate; border:0px !important;'>";
       echo"<tr>";
$counter = 0;
foreach ($sortedArray as $filename) {

if ($counter % 6 == 0) {
    echo '</tr><tr>';
	    echo "<td style='border:0px !important;'>";		
			
    echo '<input type="radio" name="selected_icon" value="'.$filename.'"><img src="'.$filename.'"   style=" height:40px;"/>';
	    echo "</td>";	
	
	
}
else{

	    echo "<td style='border:0px !important; '>";		
			
    echo '<input type="radio" name="selected_icon" value="'.$filename.'"><img src="'.$filename.'"   style=" height:40px;"/>';
	    echo "</td>";


}		
  $counter++;
  
  
}
	    /* echo "<td style='border:0px !important; '>";		
			
    echo '<input type="radio" name="selected_icon" value=""><div style="font-weight:bold; color:grey; margin-left:40px; margin-top:-25px;">None</div>';
	    echo "</td>"; */

echo "</tr></table>";
							  
						  
	           echo"<h3>Or upload new:</h3><td style='border:0px !important; '><input type='radio' name='selected_icon' value='upload' style='width:90px; margin-left:-20px;'>&nbsp;&nbsp;&nbsp;<img id='img_prev' src='preview.jpg' alt='select sprite icon' style='background-color:#62bd18; height:40px; margin-left:-40px;'/><input type='file'  id='image'  name='image_file' onchange='readURL(this);' style='width:90px;'/>";
	           echo"</td>";						  
							  
        ?>	
          <br><br>			<h3><sub>*</sub>Main Image</h3>	
		  &nbsp;&nbsp;<img src="<?php echo "crop-img/main_images/$main_image"; ?>"  style=" height:100px;"/>			
			<?php 
               echo "<br><br>&nbsp;&nbsp;<a href=\"crop-img/edit-select-img.php?id=".$id."\">Upload and crop new image</a>";	

			?>
			<input type="hidden" name="id" value="<?php echo $id; ?>"/>
			<input type="hidden" name="image_option" value="<?php echo $icon; ?>"/>
			<br>		
			<hr>	
			<div class="form-controls">
				<div class="add-button on-bottom">
								<a href="#" onclick="document.getElementById('submitID').click(); return false;"   required/>SAVE</a>
								<input type="submit" id="submitID" style="visibility: hidden;" name="submit"  />						
				</div>							
			</div>
						<div class="cancel-custom"><a href="products.php" />Cancel</a></div>
		</form>

	</section>
</body>
</html>